package com.example.yogaclass.domain.service.contracts

import com.example.yogaclass.enums.ClockState
import com.example.yogaclass.enums.ClockType

interface ClockService {

    var clockType: ClockType
    var state: ClockState
    var startTime: Long
    var pauseStartTime: Long
    var recalculatedStartTime: Long

    fun start()
    fun stop()
    fun pause()
    fun resume()
    fun changeClockType(selectedClockType: ClockType, countdownTime: Long = 0)
    fun setStartCountdownTimeSeconds(countdownTimeSeconds: Long)
}