package com.example.yogaclass.domain.mapper

interface BaseMapper<R, T> {
    fun fromDataObjectToModel(dataObject: R) : T
}