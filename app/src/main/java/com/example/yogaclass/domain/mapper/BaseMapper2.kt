package com.example.yogaclass.domain.mapper

interface BaseMapper2<R, T> : BaseMapper<R, T> {
    fun fromModelToDataObject(model: T) : R
}