package com.example.yogaclass.domain.manager.settings

import com.example.yogaclass.App
import com.example.yogaclass.AppConstants.Settings.DEFAULT_APP_THEME_ID
import com.example.yogaclass.AppConstants.Settings.SETTINGS_SECONDS_FOR_REST_0
import com.example.yogaclass.AppConstants.Settings.SETTINGS_SECONDS_FOR_REST_1
import com.example.yogaclass.AppConstants.Settings.SETTINGS_SECONDS_FOR_REST_2
import com.example.yogaclass.AppConstants.Settings.SETTINGS_SECONDS_FOR_REST_3
import com.example.yogaclass.AppConstants.Settings.SETTINGS_SECONDS_FOR_REST_4
import com.example.yogaclass.AppConstants.Settings.SETTINGS_TIME_FOR_REST_0_ID
import com.example.yogaclass.AppConstants.Settings.SETTINGS_TIME_FOR_REST_1_ID
import com.example.yogaclass.AppConstants.Settings.SETTINGS_TIME_FOR_REST_2_ID
import com.example.yogaclass.AppConstants.Settings.SETTINGS_TIME_FOR_REST_3_ID
import com.example.yogaclass.AppConstants.Settings.SETTINGS_TIME_FOR_REST_4_ID
import com.example.yogaclass.AppConstants.Settings.THEME_ID_BASIL
import com.example.yogaclass.AppConstants.Settings.THEME_ID_LAVENDER
import com.example.yogaclass.AppConstants.Settings.THEME_ID_PURPLE
import com.example.yogaclass.AppConstants.Settings.THEME_ID_RED
import com.example.yogaclass.R
import com.example.yogaclass.dal.prefs.SharedPreferenceManager

class SettingsManagerImpl(private val _sharedPref: SharedPreferenceManager) : SettingsManager {
    override fun getSecondsForRest(timeForRestId: String): Int {
        return when (timeForRestId) {
            SETTINGS_TIME_FOR_REST_1_ID -> SETTINGS_SECONDS_FOR_REST_1
            SETTINGS_TIME_FOR_REST_2_ID -> SETTINGS_SECONDS_FOR_REST_2
            SETTINGS_TIME_FOR_REST_3_ID -> SETTINGS_SECONDS_FOR_REST_3
            SETTINGS_TIME_FOR_REST_4_ID -> SETTINGS_SECONDS_FOR_REST_4
            else -> SETTINGS_SECONDS_FOR_REST_0
        }
    }

    override fun getIdOfSelectedTimeForRest(): String? {
        return _sharedPref
            .getString(App.instance.resources.getString(R.string.settings_time_for_rest_key),
                SETTINGS_TIME_FOR_REST_0_ID)
    }

    override fun getAppThemeId(): String? {
        return _sharedPref
            .getString(App.instance.resources.getString(R.string.settings_app_theme_key),
                DEFAULT_APP_THEME_ID)
    }

    override fun getAppThemeName(themeId: String): String {

        return when (themeId) {
            THEME_ID_LAVENDER -> App.instance.getString(R.string.settings_app_theme_lavender)
            THEME_ID_PURPLE -> App.instance.getString(R.string.settings_app_theme_purple)
            THEME_ID_BASIL -> App.instance.getString(R.string.settings_app_theme_basil)
            THEME_ID_RED -> App.instance.getString(R.string.settings_app_theme_red)
            else -> App.instance.getString(R.string.settings_app_theme_peach)
        }
    }

    override fun getSelectedTimeForRest(timeForRestId: String): String {
        return when (timeForRestId) {
            SETTINGS_TIME_FOR_REST_1_ID -> App.instance.getString(R.string.settings_time_for_rest_1)
            SETTINGS_TIME_FOR_REST_2_ID -> App.instance.getString(R.string.settings_time_for_rest_2)
            SETTINGS_TIME_FOR_REST_3_ID -> App.instance.getString(R.string.settings_time_for_rest_3)
            SETTINGS_TIME_FOR_REST_4_ID -> App.instance.getString(R.string.settings_time_for_rest_4)
            else -> App.instance.getString(R.string.settings_time_for_rest_0)
        }
    }
}