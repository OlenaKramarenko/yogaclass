package com.example.yogaclass.domain.manager.settings

interface SettingsManager {
    fun getAppThemeId(): String?

    fun getIdOfSelectedTimeForRest(): String?

    fun getAppThemeName(themeId: String): String

    fun getSelectedTimeForRest(timeForRestId: String): String

    fun getSecondsForRest(timeForRestId: String): Int
}