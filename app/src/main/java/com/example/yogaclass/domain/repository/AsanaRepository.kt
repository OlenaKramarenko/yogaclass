package com.example.yogaclass.domain.repository

import androidx.lifecycle.LiveData
import com.example.yogaclass.enums.AsanaCategory
import com.example.yogaclass.model.Asana
import com.example.yogaclass.network.util.NetworkResult

interface AsanaRepository {
    suspend fun getFullAsana(url: String): NetworkResult<Asana>
    suspend fun getFullAsanaFromNetwork(url: String, previewUrl: String): NetworkResult<Asana>
    suspend fun getAllAsanas(): NetworkResult<List<AsanaCategory>>

    fun getLiveDataAsanasBy(category: AsanaCategory): LiveData<List<Asana>>

}