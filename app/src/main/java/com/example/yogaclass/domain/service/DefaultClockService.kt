package com.example.yogaclass.domain.service

import android.os.SystemClock
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.example.yogaclass.enums.ClockState
import com.example.yogaclass.enums.ClockType
import com.example.yogaclass.domain.event.TimeStateEventArgs
import com.example.yogaclass.domain.event.TimeProgressEventArgs
import com.example.yogaclass.domain.service.contracts.ClockService
import com.example.yogaclass.util.CountdownTimer
import com.example.yogaclass.util.DateTimeUtil.getMillisFromSeconds
import com.example.yogaclass.util.Stopwatch
import com.example.yogaclass.util.contract.Timer
import java.lang.ref.WeakReference


class DefaultClockService(
    /*lifecycle: Lifecycle,*/
    private var timerStateChanged: ((TimeStateEventArgs) -> Unit),
    private var timerProgress: ((TimeProgressEventArgs) -> Unit),
    countdownTimeSeconds: Long = Long.MAX_VALUE,
    _clockType: ClockType = ClockType.STOPWATCH
) : ClockService, LifecycleObserver {

    private var _countdownTime: Long = Long.MAX_VALUE
    private var _leftCountdownTime: Long = Long.MAX_VALUE

    private var _interval: Long = 1000

    override var clockType: ClockType = _clockType
    override var state: ClockState = ClockState.Stop
    override var startTime: Long = 0
    override var pauseStartTime: Long = 0
    override var recalculatedStartTime: Long = 0

    private var _timer: CountdownTimer? = null
    private var _stopwatch: Stopwatch? = null

    init {
        //lifecycle.addObserver(this)

        if(countdownTimeSeconds != Long.MAX_VALUE)
            setStartCountdownTimeSeconds(countdownTimeSeconds)
    }

    override fun start() {
        clockDispose()

        //Timber.e("---------------------  ClockService.start $clockType $_countdownTime ---------------------------------------")
        when(clockType) {
            ClockType.STOPWATCH -> _stopwatch = setupNewStopwatch()
            ClockType.TIMER -> _timer = setupNewTimer(_countdownTime)
        }

        startTimer()
    }

    private fun startTimer() {
        //Timber.e("---------------------  ClockService.startTimer $state $clockType $_timer ---------------------------------------")
        startTime = SystemClock.elapsedRealtime()
        recalculatedStartTime = startTime

        notifyStateChanged(ClockState.Start)

        when(clockType) {
            ClockType.STOPWATCH -> _stopwatch?.start()
            ClockType.TIMER -> _timer?.start()
        }

        state = ClockState.Running

        notifyStateChanged(ClockState.Running)
    }

    private fun setupNewStopwatch(): Stopwatch
    {
        return object : Stopwatch(_interval) {
            override val timerRef: WeakReference<Timer> = WeakReference(this)
            override var handler: TimerHandler = TimerHandler(timerRef)

            override fun onFinish() {
                notifyStateChanged(ClockState.Stop)
            }

            override fun onTick(signalTime: Long, millisLeft: Long) {
                timerElapsed(signalTime)
            }
        }
    }

    private fun setupNewTimer(countDownTime: Long): CountdownTimer
    {
        return object : CountdownTimer(countDownTime, _interval) {
            override val timerRef: WeakReference<Timer> = WeakReference(this)
            override var handler: TimerHandler = TimerHandler(timerRef)


            override fun onFinish() {
                //Timber.e("---------------------  CountdownTimer.onFinish ---------------------------------------")
                notifyStateChanged(ClockState.Stop)
            }

            override fun onTick(signalTime: Long, millisLeft: Long) {
                //Timber.e("---------------------  CountdownTimer.onTick ---------------------------------------")
                timerElapsed(signalTime, millisLeft)
            }
        }
    }

    private fun timerElapsed(signalTime: Long, millisLeft: Long = 0)
    {
        val elapsedTime = signalTime - recalculatedStartTime
        val timeProgressEventArgs = TimeProgressEventArgs( millisPassed = elapsedTime, started = startTime )
        if(clockType == ClockType.TIMER){
            timeProgressEventArgs.millisLeft = millisLeft
        }
        _leftCountdownTime = millisLeft
        timerProgress.invoke(timeProgressEventArgs);
    }


    override fun stop() {
        //Timber.e("---------------------  ClockService.stop  $state ---------------------------------------")
        clockDispose()
    }


    override fun pause() {
        state = ClockState.OnPause
        pauseStartTime = SystemClock.elapsedRealtime()
        when(clockType) {
            ClockType.STOPWATCH -> _stopwatch?.cancel()
            ClockType.TIMER ->  _timer?.cancel()
        }

        notifyStateChanged(ClockState.OnPause)
    }

    override fun resume() {
        val currentTime = SystemClock.elapsedRealtime()
        val pauseDuration = currentTime - pauseStartTime
        recalculatedStartTime += pauseDuration

        if(clockType == ClockType.TIMER){
            _timer = setupNewTimer(_leftCountdownTime)
            _timer?.start()
        } else if (clockType == ClockType.STOPWATCH){
            _stopwatch?.start()
        }

        state = ClockState.Running;

        notifyStateChanged(ClockState.Running)
    }

    private fun notifyStateChanged(state: ClockState)
    {
        val eventArgs = TimeStateEventArgs(
            state = state,
            startTime = startTime,
            stopDateTime = if(state == ClockState.Stop) SystemClock.elapsedRealtime() else 0
            )

        timerStateChanged.invoke(eventArgs);
    }

    override fun changeClockType(selectedClockType: ClockType, countdownTime: Long){
        when(selectedClockType){
            ClockType.STOPWATCH -> setClockTypeAsStopwatch()
            ClockType.TIMER -> setClockTypeAsTimer(countdownTime)
        }
    }

    private fun setClockTypeAsTimer(countdownTimeSeconds: Long){
        //Timber.e("---------------------  ClockService.setClockTypeAsTimer  $state ---------------------------------------")
        clockDispose()

        setStartCountdownTimeSeconds(getMillisFromSeconds(countdownTimeSeconds))
        clockType = ClockType.TIMER
    }

    private fun setClockTypeAsStopwatch(){
        //Timber.e("---------------------  ClockService.setClockTypeAsStopwatch  $state ---------------------------------------")
        clockDispose()

        setStartCountdownTimeSeconds(0)
        clockType = ClockType.STOPWATCH
    }

    override fun setStartCountdownTimeSeconds(countdownTimeSeconds: Long) {
        this._countdownTime = getMillisFromSeconds(countdownTimeSeconds)
        this._leftCountdownTime = this._countdownTime
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun clockServiceDispose(){
        clockDispose()
    }

    private fun clockDispose(){
        //Timber.e("---------------------  ClockService.clockDispose  $state ---------------------------------------")
        state = ClockState.Stop

        _timer?.cancel()
        _timer = null
        _stopwatch?.cancel()
        _stopwatch = null
        this._leftCountdownTime = this._countdownTime
    }
}