package com.example.yogaclass.domain.event

import com.example.yogaclass.enums.ClockState

data class TimeStateEventArgs(
    var state: ClockState,
    var startTime: Long,
    var stopDateTime: Long = 0
)

data class TimeProgressEventArgs(
    var started: Long,
    var millisPassed: Long,
    var millisLeft: Long = 0
)