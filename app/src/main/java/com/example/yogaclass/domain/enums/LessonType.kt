package com.example.yogaclass.domain.enums

enum class LessonType(var filterKey: Int) {
    DEFAULT(0),
    CUSTOM(1),
}