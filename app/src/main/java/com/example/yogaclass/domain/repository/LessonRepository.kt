package com.example.yogaclass.domain.repository

import androidx.lifecycle.LiveData
import com.example.yogaclass.enums.LessonsFilterType
import com.example.yogaclass.model.Asana
import com.example.yogaclass.model.Lesson
import java.util.*

interface LessonRepository {

    suspend fun getLesson(lessonId: Int): Lesson?

    suspend fun getLessonByTitle(lessonTitle: String): Lesson?

    suspend fun getLessonsByFilterType(filter: LessonsFilterType): List<Lesson>

    suspend fun saveLesson(lesson: Lesson): Long

    suspend fun updateLessonLastUsageDate(lessonId: Int, lastUsageDate: Date)

    suspend fun addAsanasToLesson(asanas: List<Asana>?,
                                  lessonAllId: Long)


    fun getLiveDataLesson(lessonId: Int): LiveData<Lesson>

    fun getLiveDataAsanasBy(lessonId: Int): LiveData<List<Asana>>

}