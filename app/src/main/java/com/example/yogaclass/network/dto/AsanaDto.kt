package com.example.yogaclass.network.dto

import com.example.yogaclass.AppConstants.Network.Asana.SELECTOR_IMAGE_URL_ATTR
import com.example.yogaclass.AppConstants.Network.Asana.SELECTOR_IMAGE_URL_VALUE
import com.example.yogaclass.AppConstants.Network.Asana.SELECTOR_LIST_INSTRUCTIONS
import com.example.yogaclass.AppConstants.Network.Asana.SELECTOR_LIST_NAMES
import com.example.yogaclass.AppConstants.Network.Asana.SELECTOR_TITLE_VALUE
import pl.droidsonroids.jspoon.annotation.Selector

class AsanaDto {
    @Selector(value = SELECTOR_IMAGE_URL_VALUE, attr =SELECTOR_IMAGE_URL_ATTR) val imageUrl: String? = null
    @Selector(value = SELECTOR_TITLE_VALUE) val title: String? = null
    @Selector(SELECTOR_LIST_NAMES) var namesList: List<String>? = null
    @Selector(SELECTOR_LIST_INSTRUCTIONS) var instructionsList: List<String>? = null
}