package com.example.yogaclass.network.util

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.delay
import retrofit2.Response


suspend inline fun <R, reified T> execCall(callable: Deferred<Response<R>>, crossinline func: (Response<R>) -> T ): NetworkResult<T> {
    delay((100..5000L).random())
    val result = NetworkResult<T>()
    try {
        val response = callable.await()

        if (response.isSuccessful) {
            result.data = func(response)
        } else {
            result.failure = Failure(
                response.code(),
                "Server error",
                response.errorBody()?.string()
            )
        }
    } catch (e: Exception) {
        result.fromException(e)
    }
    return result
}


