package com.example.yogaclass.network.util.ext

import com.example.yogaclass.network.util.NetworkResult

fun <T> NetworkResult<List<T>>.add(result: NetworkResult<T>) {
    if(this.data == null)
        this.data = listOf()

    if(result.data != null) {
        val mutableList = mutableListOf<T>()
        mutableList.add(result.data!!)
        mutableList.addAll(this.data!!)
        this.data = mutableList
    }

    if(result.failure != null){
        if(this.failure != null) {
            this.failure!!.errorMessage = "${this.failure!!.errorMessage}, ${result.failure!!.errorMessage}"
            this.failure!!.errorData = "${this.failure!!.errorData}, ${result.failure!!.errorData}"
            if(this.failure!!.errorCode == -1) this.failure!!.errorCode = result.failure!!.errorCode
        }
        else this.failure =  result.failure
    }

    if(result.isConnectionError) this.isConnectionError = true

}

fun <T> NetworkResult<List<T>>.add(data: T) {
    if (this.data == null)
        this.data = listOf()

    val mutableList = mutableListOf<T>()
    mutableList.add(data)
    mutableList.addAll(this.data!!)
    this.data = mutableList
}
