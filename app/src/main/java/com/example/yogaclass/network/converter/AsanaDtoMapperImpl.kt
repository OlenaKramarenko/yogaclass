package com.example.yogaclass.network.converter

import com.example.yogaclass.AppConstants
import com.example.yogaclass.domain.mapper.BaseMapper
import com.example.yogaclass.model.Asana
import com.example.yogaclass.network.dto.AsanaDto

class AsanaDtoMapperImpl : BaseMapper<AsanaDto, Asana> {
    private val indexOfSanskritNameInList: Int = 0
    private val indexOfExtraNameInList: Int = 2
    private val labelExtraName: String = "also called"

    override fun fromDataObjectToModel(dataObject: AsanaDto) = Asana(
            asanaId = "",
            title = dataObject.title ?: "",
            url = "",
            previewUrl = "",
            imageUrl = dataObject.imageUrl ?: "",
            sanskritName = dataObject.namesList?.let {
                return@let it[indexOfSanskritNameInList].replace(AppConstants.Asana.STR_SANSKRIT_NAME, "")
            } ?: "",
            extraName = dataObject.namesList?.let {
                if (it[indexOfExtraNameInList].contains(labelExtraName))
                    return@let it[indexOfExtraNameInList].replace(AppConstants.Asana.STR_ALSO_CALLED, "")
                else return@let ""
            },
            instruction = dataObject.instructionsList ?: emptyList()
        )
}