package com.example.yogaclass.network.converter

import com.example.yogaclass.domain.mapper.BaseMapper
import com.example.yogaclass.model.Asana
import com.example.yogaclass.network.dto.AsanaListDto
import com.example.yogaclass.network.dto.AsanasDto
import java.util.*

class AsanasDtoMapperImpl(private var listMapper : BaseMapper<AsanaListDto, Asana>) :
    BaseMapper<AsanasDto, List<Asana>> {

    companion object {
        private const val NO_VALUE = "no_value"
    }

    override fun fromDataObjectToModel(dataObject: AsanasDto): List<Asana> {
        return dataObject.asanaLists
            ?.asSequence()
            ?.map { asanaDto -> listMapper.fromDataObjectToModel(asanaDto) }
            ?.filter { asana ->  asana.url.toLowerCase(Locale.US) != NO_VALUE }
            ?.toList()
            ?: emptyList()
    }
}