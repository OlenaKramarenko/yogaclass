package com.example.yogaclass.network.converter

import com.example.yogaclass.domain.mapper.BaseMapper
import com.example.yogaclass.model.Asana
import com.example.yogaclass.network.dto.AsanaListDto

class AsanaListMapperImpl : BaseMapper<AsanaListDto, Asana> {

    override fun fromDataObjectToModel(dataObject: AsanaListDto): Asana {
        return Asana(
            dataObject.url?: "",
            dataObject.title ?: "",
            dataObject.url?: "",
            dataObject.previewUrl ?: "")
    }
}