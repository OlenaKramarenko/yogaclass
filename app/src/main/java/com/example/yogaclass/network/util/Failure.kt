package com.example.yogaclass.network.util

data class Failure(
    var httpCode: Int = -1,
    var errorMessage: String = "",
    var errorData: String? = "",
    var errorCode: Int = -1
)