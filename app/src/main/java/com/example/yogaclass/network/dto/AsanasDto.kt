package com.example.yogaclass.network.dto

import com.example.yogaclass.AppConstants.Network.AsanaList.SELECTOR_LIST
import pl.droidsonroids.jspoon.annotation.Selector

class AsanasDto {
    @Selector(SELECTOR_LIST) var asanaLists: List<AsanaListDto>? = null
}