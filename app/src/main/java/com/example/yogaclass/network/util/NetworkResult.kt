package com.example.yogaclass.network.util

import com.example.yogaclass.AppConstants.Error.CODE_CONNECTION_ERROR
import com.example.yogaclass.AppConstants.Error.CODE_HTTP_ERROR
import com.example.yogaclass.AppConstants.Error.CODE_SERVER_ERROR
import com.example.yogaclass.AppConstants.Error.CODE_UNKNOWN_ERROR
import com.example.yogaclass.AppConstants.Error.STR_CONNECTION_ERROR
import com.example.yogaclass.AppConstants.Error.STR_SERVER_ERROR
import com.example.yogaclass.AppConstants.Error.STR_UNKNOWN_ERROR
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

open class NetworkResult<T> {
    var data: T? = null
    var failure: Failure? = null
    var isConnectionError = false

    fun fromRetrofitResponse(response: Response<T>, func: () -> T ) {
        if (response.isSuccessful) {
            data = func()
        } else {
            failure = Failure(
                response.code(),
                STR_SERVER_ERROR,
                response.errorBody()?.string(),
                CODE_SERVER_ERROR
            )
        }
    }

    fun fromException(e: Exception) {
        if (e is HttpException) {
            failure = Failure(e.code(), e.message(), "", CODE_HTTP_ERROR)
        } else if (e is IOException) {
            failure = Failure(0, e.message ?: STR_CONNECTION_ERROR, "", CODE_CONNECTION_ERROR)
            isConnectionError = true
        } else {
            failure = Failure(0, e.message ?: STR_UNKNOWN_ERROR, "", CODE_UNKNOWN_ERROR)
        }
    }

    fun completedSuccessfully() = data != null

}
