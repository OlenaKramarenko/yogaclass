package com.example.yogaclass.network.services

import com.example.yogaclass.AppConstants.Network.AsanaList.LINK_BEGINNER_YOGA_SEQUENCE
import com.example.yogaclass.AppConstants.Network.AsanaList.LINK_ESSENTIAL_YOGA_POSES
import com.example.yogaclass.AppConstants.Network.AsanaList.LINK_LONG_AND_LEAN_FULL_BODY
import com.example.yogaclass.network.dto.AsanaDto
import com.example.yogaclass.network.dto.AsanasDto
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface DataService {
    @GET(LINK_ESSENTIAL_YOGA_POSES)
    fun essentialYogaAsanas(): Deferred<Response<AsanasDto>>

    @GET(LINK_BEGINNER_YOGA_SEQUENCE)
    fun beginnerYogaAsanas(): Deferred<Response<AsanasDto>>

    @GET(LINK_LONG_AND_LEAN_FULL_BODY)
    fun asanasForLongAndLeanFullBody(): Deferred<Response<AsanasDto>>

    @GET
    fun getAsanaBy(@Url url: String): Deferred<Response<AsanaDto>>
}