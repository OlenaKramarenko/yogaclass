package com.example.yogaclass.network.dto

import com.example.yogaclass.AppConstants.Network.AsanaList.SELECTOR_IMAGE_URL_ATTR
import com.example.yogaclass.AppConstants.Network.AsanaList.SELECTOR_TITLE_ATTR
import com.example.yogaclass.AppConstants.Network.AsanaList.SELECTOR_URL_ATTR
import com.example.yogaclass.AppConstants.Network.AsanaList.SELECTOR_URL_VALUE
import com.example.yogaclass.AppConstants.Network.AsanaList.SELECTOR_VALUE
import pl.droidsonroids.jspoon.annotation.Selector

class AsanaListDto {
    @Selector(value = SELECTOR_URL_VALUE, attr = SELECTOR_URL_ATTR) val url: String? = null
    @Selector(value = SELECTOR_VALUE, attr = SELECTOR_TITLE_ATTR) val title: String? = null
    @Selector(value = SELECTOR_VALUE, attr = SELECTOR_IMAGE_URL_ATTR) val previewUrl: String? = null
}