package com.example.yogaclass.model

import com.example.yogaclass.enums.AsanaCategory

data class Asana (
    var asanaId: String,
    val title: String,
    var url: String,
    var previewUrl: String? = null,
    val imageUrl: String? = null,
    val sanskritName: String? = null,
    val extraName: String? = null,
    val instruction: List<String>? = null,
    var category: AsanaCategory = AsanaCategory.ESSENTIAL_YOGA_POSES,

    val selected: Boolean = false
)