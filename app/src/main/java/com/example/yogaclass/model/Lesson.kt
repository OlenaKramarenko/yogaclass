package com.example.yogaclass.model

import com.example.yogaclass.domain.enums.LessonType
import java.util.*

data class Lesson(
    val id: Int,
    val title: String,
    val creationDate: Date,
    val lastUsageDate: Date,
    var asanas: List<Asana>? = null,
    val lessonType: LessonType
    )