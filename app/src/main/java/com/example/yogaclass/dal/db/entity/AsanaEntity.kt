package com.example.yogaclass.dal.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.yogaclass.enums.AsanaCategory

@Entity(
    tableName = "asana"
)
data class AsanaEntity (

    @PrimaryKey
    @ColumnInfo(name = "id", index = true)
    var id: String,

    @ColumnInfo(name = "title")
    val title: String,

    @ColumnInfo(name = "url")
    var url: String,

    @ColumnInfo(name = "preview_url")
    val previewUrl: String,

    @ColumnInfo(name = "image_url")
    val imageUrl: String,

    @ColumnInfo(name = "sanskrit_name")
    val sanskritName: String,

    @ColumnInfo(name = "extra_name")
    val extraName: String,

    @ColumnInfo(name = "instruction")
    val instruction: List<String>,

    @ColumnInfo(name = "category", index = true)
    val category: AsanaCategory

)