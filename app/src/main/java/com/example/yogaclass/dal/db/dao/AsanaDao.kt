package com.example.yogaclass.dal.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.yogaclass.dal.db.entity.AsanaEntity
import com.example.yogaclass.enums.AsanaCategory

@Dao
interface AsanaDao {

    @Insert(onConflict = IGNORE)
    suspend fun insertAsanas(asanas: List<AsanaEntity>)

    @Insert(onConflict = REPLACE)
    suspend fun insertAsana(asana: AsanaEntity)

    @Query("SELECT * from asana where id = :asanaId")
    fun getAsana(asanaId: String): AsanaEntity?

    @Query("SELECT * from asana order by id")
    fun getAllAsanas(): List<AsanaEntity>

    @Query("SELECT * from asana where category = :category order by id")
    fun getAsanasBy(category: AsanaCategory): List<AsanaEntity>


    @Query("SELECT * from asana order by id")
    fun getAllLiveDataAsanas(): LiveData<List<AsanaEntity>>

    @Query("SELECT * from asana where category = :category order by id")
    fun getLiveDataAsanasBy(category: AsanaCategory): LiveData<List<AsanaEntity>>
}