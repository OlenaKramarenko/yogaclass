package com.example.yogaclass.dal.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.yogaclass.dal.db.entity.AsanaEntity
import com.example.yogaclass.dal.db.entity.LessonAsanasEntity

@Dao
interface LessonAsanaDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertLessonAsana(asana: LessonAsanasEntity)

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("SELECT * from asana INNER JOIN lesson_asanas ON asana.id = lesson_asanas.asanaId where lesson_asanas.lessonId = :lessonId")
    fun getAsanasBy(lessonId: Int): List<AsanaEntity>


    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @Query("SELECT * from asana INNER JOIN lesson_asanas ON asana.id = lesson_asanas.asanaId where lesson_asanas.lessonId = :lessonId")
    fun getAsanasLiveDataBy(lessonId: Int): LiveData<List<AsanaEntity>>
}