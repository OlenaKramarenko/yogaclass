package com.example.yogaclass.dal.db.converter

import androidx.room.TypeConverter
import com.example.yogaclass.ext.toDate
import com.example.yogaclass.ext.toTimeStamp
import java.util.*

class DateConverter {

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else value.toDate()
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.toTimeStamp()
    }

}