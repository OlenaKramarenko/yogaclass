package com.example.yogaclass.dal.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.yogaclass.dal.db.converter.DateConverter
import com.example.yogaclass.dal.db.converter.EnumConverter
import com.example.yogaclass.dal.db.converter.StringConverter
import com.example.yogaclass.dal.db.dao.AsanaDao
import com.example.yogaclass.dal.db.dao.LessonAsanaDao
import com.example.yogaclass.dal.db.dao.LessonDao
import com.example.yogaclass.dal.db.entity.AsanaEntity
import com.example.yogaclass.dal.db.entity.LessonAsanasEntity
import com.example.yogaclass.dal.db.entity.LessonEntity

@Database(entities = [AsanaEntity::class, LessonEntity::class, LessonAsanasEntity::class], version = 1, exportSchema = false)
@TypeConverters(StringConverter::class, EnumConverter::class, DateConverter::class)
abstract class AppDatabase : RoomDatabase(){

    abstract fun asanaDao(): AsanaDao

    abstract fun lessonDao(): LessonDao

    abstract fun lessonAsanaDao(): LessonAsanaDao
}