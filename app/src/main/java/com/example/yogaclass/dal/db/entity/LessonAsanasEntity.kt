package com.example.yogaclass.dal.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    tableName = "lesson_asanas",
    primaryKeys = arrayOf("lessonId", "asanaId"),
    foreignKeys = arrayOf(
        ForeignKey(entity = LessonEntity::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("lessonId")),
        ForeignKey(entity = AsanaEntity::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("asanaId"))

    )
)
data class LessonAsanasEntity(

    val lessonId: Long,

    @ColumnInfo(index = true)
    val asanaId: String
)