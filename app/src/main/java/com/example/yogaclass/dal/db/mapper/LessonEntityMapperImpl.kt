package com.example.yogaclass.dal.db.mapper

import com.example.yogaclass.dal.db.entity.LessonEntity
import com.example.yogaclass.domain.mapper.BaseMapper2
import com.example.yogaclass.model.Lesson
import com.example.yogaclass.ext.toDate

class LessonEntityMapperImpl : BaseMapper2<LessonEntity, Lesson> {
    override fun fromDataObjectToModel(dataObject: LessonEntity): Lesson {
        return Lesson(
            dataObject.id,
            dataObject.title,
            dataObject.creationDate.toDate(),
            dataObject.lastUsageDate.toDate(),
            lessonType = dataObject.lessonType
        )
    }

    override fun fromModelToDataObject(model: Lesson): LessonEntity {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}