package com.example.yogaclass.dal.db.converter

import androidx.room.TypeConverter
import com.example.yogaclass.enums.AsanaCategory
import com.example.yogaclass.domain.enums.LessonType
import com.google.gson.Gson

class EnumConverter {

    @TypeConverter
    fun fromAsanaCategory(category: AsanaCategory) : String = Gson().toJson(category)

    @TypeConverter
    fun toAsanaCategory(value: String) : AsanaCategory = Gson().fromJson(value, AsanaCategory::class.java) as AsanaCategory

    @TypeConverter
    fun fromLessonType(lessonType: LessonType) : String = Gson().toJson(lessonType)

    @TypeConverter
    fun toLessonType(value: String) : LessonType = Gson().fromJson(value, LessonType::class.java) as LessonType

}