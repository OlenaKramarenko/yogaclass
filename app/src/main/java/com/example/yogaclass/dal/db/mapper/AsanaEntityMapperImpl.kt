package com.example.yogaclass.dal.db.mapper

import com.example.yogaclass.dal.db.entity.AsanaEntity
import com.example.yogaclass.model.Asana
import com.example.yogaclass.domain.mapper.BaseMapper2

class AsanaEntityMapperImpl : BaseMapper2<AsanaEntity, Asana> {
    override fun fromModelToDataObject(model: Asana): AsanaEntity {
        return AsanaEntity(
            model.asanaId,
            model.title,
            model.url,
            model.previewUrl ?: "",
            model.imageUrl ?: "",
            model.sanskritName ?: "",
            model.extraName ?: "",
            model.instruction ?: emptyList(),
            model.category
        )
    }

    override fun fromDataObjectToModel(dataObject: AsanaEntity): Asana {
        return Asana(
            dataObject.id,
            dataObject.title,
            dataObject.url,
            dataObject.previewUrl,
            dataObject.imageUrl,
            dataObject.sanskritName,
            dataObject.extraName,
            dataObject.instruction,
            dataObject.category
        )
    }
}