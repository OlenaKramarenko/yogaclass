package com.example.yogaclass.dal.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.yogaclass.dal.db.entity.LessonEntity

@Dao
interface LessonDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertLesson(lesson: LessonEntity) : Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateLesson(lesson: LessonEntity)

    @Query("SELECT * from lesson where id = :lessonId")
    fun getLesson(lessonId: Int): LessonEntity?

    @Query("SELECT * from lesson where title = :lessonTitle")
    fun getLessonByTitle(lessonTitle: String): LessonEntity?

    @Query("SELECT * from lesson")
    fun getAllLessons(): List<LessonEntity>

    @Query("SELECT * from lesson order by title")
    fun getAllLessonsOrderedByTitle(): List<LessonEntity>

    @Query("SELECT * from lesson order by creation_date Desc")
    fun getAllLessonsOrderedByCreationDate(): List<LessonEntity>

    @Query("SELECT * from lesson order by last_usage_date Desc")
    fun getAllLessonsOrderedByLastUsageDate(): List<LessonEntity>


    @Query("SELECT * from lesson where id = :lessonId")
    fun getLessonLiveData(lessonId: Int): LiveData<LessonEntity>
}