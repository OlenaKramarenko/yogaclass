package com.example.yogaclass.dal.db.converter

import androidx.room.TypeConverter
import com.google.gson.Gson

class StringConverter {

    @TypeConverter
    fun fromList(list: List<String>?) : String = Gson().toJson(list)

    @TypeConverter
    fun toList(value: String) : List<String>?
    {
        val objects = Gson().fromJson(value, Array<String>::class.java) as Array<String>
        return objects.toList()
    }

}