package com.example.yogaclass.dal.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.yogaclass.domain.enums.LessonType

@Entity(
    tableName = "lesson"
)
data class LessonEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id", index = true)
    var id: Int,

    @ColumnInfo(name = "title")
    val title: String,

    @ColumnInfo(name = "creation_date")
    val creationDate: Long,

    @ColumnInfo(name = "last_usage_date")
    var lastUsageDate: Long,

    @ColumnInfo(name = "lesson_type")
    val lessonType: LessonType
)