package com.example.yogaclass.dal.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.yogaclass.dal.db.dao.LessonAsanaDao
import com.example.yogaclass.dal.db.dao.LessonDao
import com.example.yogaclass.dal.db.entity.AsanaEntity
import com.example.yogaclass.dal.db.entity.LessonAsanasEntity
import com.example.yogaclass.dal.db.entity.LessonEntity
import com.example.yogaclass.dal.db.mapper.LessonEntityMapperImpl
import com.example.yogaclass.domain.mapper.BaseMapper2
import com.example.yogaclass.domain.repository.LessonRepository
import com.example.yogaclass.enums.LessonsFilterType
import com.example.yogaclass.ext.toTimeStamp
import com.example.yogaclass.model.Asana
import com.example.yogaclass.model.Lesson
import java.util.*

class LessonRepositoryImpl(private val _lessonDao: LessonDao,
                           private val _lessonAsanaDao: LessonAsanaDao,
                           private val _lessonEntityMapper: LessonEntityMapperImpl,
                           private  val _asanaEntityMapper: BaseMapper2<AsanaEntity, Asana>
) : LessonRepository {

    override suspend fun updateLessonLastUsageDate(lessonId: Int, lastUsageDate: Date) {
        val lessonEntity = _lessonDao.getLesson(lessonId) ?: return

        lessonEntity.lastUsageDate = lastUsageDate.toTimeStamp()
        _lessonDao.updateLesson(lessonEntity)
    }

    override fun getLiveDataAsanasBy(lessonId: Int): LiveData<List<Asana>> =
                         Transformations.map(_lessonAsanaDao.getAsanasLiveDataBy(lessonId))
                                                 { asanas -> asanas.map { asanaEntity ->  getAsanaModel(asanaEntity) }}

    override fun getLiveDataLesson(lessonId: Int): LiveData<Lesson> =
        Transformations.map(_lessonDao.getLessonLiveData(lessonId))
                                                        { lesson -> _lessonEntityMapper.fromDataObjectToModel(lesson)}

    override suspend fun saveLesson(lesson: Lesson): Long {
        val lessonId = _lessonDao.insertLesson(LessonEntity(0, lesson.title,
                                                                    lesson.creationDate.toTimeStamp(),
                                                                    lesson.lastUsageDate.toTimeStamp(),
                                                                    lesson.lessonType))
        addAsanasToLesson(lesson.asanas, lessonId)

        return lessonId
    }

    override suspend fun addAsanasToLesson(asanas: List<Asana>?,
                                           lessonAllId: Long) {
        asanas?.forEach{
                asana -> _lessonAsanaDao.insertLessonAsana(LessonAsanasEntity(lessonAllId, asana.asanaId))
        }
    }

    override suspend fun getLesson(lessonId: Int): Lesson? {
        val lessonEntity = _lessonDao.getLesson(lessonId) ?: return null

        return convertLessonEntityToLesson(lessonEntity)
    }

    override suspend fun getLessonByTitle(lessonTitle: String): Lesson? {
        val lessonEntity = _lessonDao.getLessonByTitle(lessonTitle)

        return if(lessonEntity != null) convertLessonEntityToLesson(lessonEntity) else null
    }

    override suspend fun getLessonsByFilterType(filter: LessonsFilterType): List<Lesson> {

        val lessonEntities = when(filter){
            LessonsFilterType.NAME -> _lessonDao.getAllLessonsOrderedByTitle()
            LessonsFilterType.CREATION_DATE -> _lessonDao.getAllLessonsOrderedByCreationDate()
            LessonsFilterType.LAST_USAGE_DATE -> _lessonDao.getAllLessonsOrderedByLastUsageDate()
        }

        return lessonEntities
            .map { lessonEntity -> convertLessonEntityToLesson(lessonEntity)}
    }

    private fun convertLessonEntityToLesson(lessonEntity: LessonEntity) : Lesson{
        val asanasForLesson = _lessonAsanaDao.getAsanasBy(lessonEntity.id)
        val lesson = _lessonEntityMapper.fromDataObjectToModel(lessonEntity)
        lesson.asanas = asanasForLesson.map { asanaEntity ->  getAsanaModel(asanaEntity) }
        return lesson
    }

    private fun getAsanaModel(asanaEntity: AsanaEntity): Asana{
        return _asanaEntityMapper.fromDataObjectToModel(asanaEntity)
    }
}