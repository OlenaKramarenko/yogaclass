package com.example.yogaclass.dal.prefs

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class SharedPreferenceManager(context: Context, name: String? = null) {
    private val sharedPreferences: SharedPreferences by lazy {
        if (name.isNullOrEmpty()) {
            PreferenceManager.getDefaultSharedPreferences(context)
        } else {
            context.getSharedPreferences(name, Context.MODE_PRIVATE)
        }
    }

    fun putString(key: String, value: String) {

        sharedPreferences.edit()?.putString(key, value)?.apply()
    }

    fun getString(key: String, defaultValue: String): String? =
        sharedPreferences.getString(key, defaultValue)

    fun putStringSet(key: String, value: List<String>) {

        val set = HashSet<String>()
        set.addAll(value)

        sharedPreferences.edit()?.putStringSet(key, set)?.apply()
    }

    fun addStringToSet(key: String, value: String) {

        val set = sharedPreferences.getStringSet(key, HashSet<String>())
        if (set?.contains(value) != true) {
            set?.add(value)
        }

        sharedPreferences
            .edit()
            ?.clear()
            ?.putStringSet(key, set)
            ?.apply()
    }

    fun getStringSet(key: String): MutableSet<String>? {
        val set = sharedPreferences.getStringSet(key, HashSet<String>())
        return set
    }
}