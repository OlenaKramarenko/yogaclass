package com.example.yogaclass.dal.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.yogaclass.AppConstants.KEY_SAVED_CATEGORIES
import com.example.yogaclass.dal.db.dao.AsanaDao
import com.example.yogaclass.dal.db.entity.AsanaEntity
import com.example.yogaclass.dal.prefs.SharedPreferenceManager
import com.example.yogaclass.domain.enums.LessonType
import com.example.yogaclass.domain.mapper.BaseMapper2
import com.example.yogaclass.domain.repository.AsanaRepository
import com.example.yogaclass.domain.repository.LessonRepository
import com.example.yogaclass.enums.AsanaCategory
import com.example.yogaclass.model.Asana
import com.example.yogaclass.model.Lesson
import com.example.yogaclass.network.converter.AsanaDtoMapperImpl
import com.example.yogaclass.network.converter.AsanasDtoMapperImpl
import com.example.yogaclass.network.dto.AsanaDto
import com.example.yogaclass.network.dto.AsanasDto
import com.example.yogaclass.network.services.DataService
import com.example.yogaclass.network.util.NetworkResult
import com.example.yogaclass.network.util.execCall
import com.example.yogaclass.network.util.ext.add
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.Response
import java.util.*

class AsanaRepositoryImpl(
    private val _service: DataService,
    private val _asanasDtoMapper: AsanasDtoMapperImpl,
    private val _asanaDtoMapper: AsanaDtoMapperImpl,
    private val _asanaDao: AsanaDao,
    private val _asanaEntityMapper: BaseMapper2<AsanaEntity, Asana>,
    private val _sharedPref: SharedPreferenceManager,
    private val _lessonRepo: LessonRepository
) : AsanaRepository {

    override fun getLiveDataAsanasBy(category: AsanaCategory): LiveData<List<Asana>> =
        Transformations.map(manageCategoryGetAsanas(category)) { asanas ->
            convertAsanaEntityToModel(
                asanas
            )
        }

    private fun manageCategoryGetAsanas(category: AsanaCategory): LiveData<List<AsanaEntity>> {
        return if (category == AsanaCategory.ALL)
            _asanaDao.getAllLiveDataAsanas()
        else
            _asanaDao.getLiveDataAsanasBy(category)
    }

    private fun convertAsanaEntityToModel(asanaEntities: List<AsanaEntity>): List<Asana> {
        return asanaEntities
            .map { asanaEntity -> _asanaEntityMapper.fromDataObjectToModel(asanaEntity) }
    }


    override suspend fun getFullAsanaFromNetwork(
        url: String,
        previewUrl: String
    ): NetworkResult<Asana> {
        return execCall(_service.getAsanaBy(url))
        { response: Response<AsanaDto> -> convertAndSaveAsana(response, url, previewUrl) }
    }

    private val jobSaveAssanas = SupervisorJob()
    private val _bgSaveScope = CoroutineScope(Dispatchers.IO + jobSaveAssanas)

    override suspend fun getFullAsana(url: String): NetworkResult<Asana> {
        val asanaEntity = _asanaDao.getAsana(url)
        if (asanaEntity?.imageUrl?.isNotEmpty() == true) {
            val result = NetworkResult<Asana>()
            result.data = _asanaEntityMapper.fromDataObjectToModel(asanaEntity)
            return result
        }

        return getFullAsanaFromNetwork(url, asanaEntity?.previewUrl ?: "")
    }

    private fun convertAndSaveAsana(
        response: Response<AsanaDto>,
        url: String,
        previewUrl: String
    ): Asana {
        val asana = convertAsanaDtoToModel(response, url, previewUrl)

        val asanaEntity = _asanaEntityMapper.fromModelToDataObject(asana)
        _bgSaveScope.launch {
            _asanaDao.insertAsana(asanaEntity)
        }

        return asana
    }

    private fun convertAsanaDtoToModel(
        response: Response<AsanaDto>,
        url: String,
        previewUrl: String
    ): Asana {
        val asana = _asanaDtoMapper.fromDataObjectToModel(response.body() ?: AsanaDto())
        asana.asanaId = url
        asana.url = url
        if (previewUrl.isNotBlank())
            asana.previewUrl = previewUrl
        return asana
    }


    override suspend fun getAllAsanas(): NetworkResult<List<AsanaCategory>> {
        val savedCategories = _sharedPref.getStringSet(KEY_SAVED_CATEGORIES)
        var networkResult: NetworkResult<List<AsanaCategory>> = NetworkResult()

        val lessonAll: Lesson? = _lessonRepo.getLessonByTitle(AsanaCategory.ALL.name)

        val lessonAllId = lessonAll?.id ?: saveDefaultLessonToDb(
            emptyList(),
            AsanaCategory.getCategoryNameBy(AsanaCategory.ALL.filterKey)
        ).toInt()

        networkResult = addDataToNetWorkResult(
            savedCategories, networkResult,
            AsanaCategory.ESSENTIAL_YOGA_POSES, lessonAllId
        )
        networkResult = addDataToNetWorkResult(
            savedCategories, networkResult,
            AsanaCategory.BEGINNER_YOGA_SEQUENCE, lessonAllId
        )
        networkResult = addDataToNetWorkResult(
            savedCategories, networkResult,
            AsanaCategory.LONG_AND_LEAN_FULL_BODY, lessonAllId
        )
        return networkResult
    }

    private suspend fun addDataToNetWorkResult(
        savedCategories: MutableSet<String>?,
        networkResult: NetworkResult<List<AsanaCategory>>,
        category: AsanaCategory,
        lessonAllId: Int
    ): NetworkResult<List<AsanaCategory>> {
        if (savedCategories?.contains(category.name) == true) {

            networkResult.add(category)
        } else {

            val asanas = when (category) {
                AsanaCategory.ESSENTIAL_YOGA_POSES -> _service.essentialYogaAsanas()
                AsanaCategory.BEGINNER_YOGA_SEQUENCE -> _service.beginnerYogaAsanas()
                AsanaCategory.LONG_AND_LEAN_FULL_BODY -> _service.asanasForLongAndLeanFullBody()
                else -> return networkResult
            }
            networkResult.add(execCall(asanas)
            { response: Response<AsanasDto> ->
                convertFromNetworkResponseAndSaveAsanas(
                    response,
                    category,
                    lessonAllId
                )
            })
        }

        return networkResult
    }

    private fun convertFromNetworkResponseAndSaveAsanas(
        response: Response<AsanasDto>, category: AsanaCategory,
        lessonAllId: Int
    ): AsanaCategory {
        val asanas = convertAsanasDtoToModelList(response, category)

        val asanaEntities = asanas.map { asana -> _asanaEntityMapper.fromModelToDataObject(asana) }

        _bgSaveScope.launch {
            saveAsanasToDb(asanaEntities, lessonAllId)
            saveDefaultLessonToDb(
                asanaEntities,
                AsanaCategory.getCategoryNameBy(category.filterKey)
            )

            _sharedPref.addStringToSet(KEY_SAVED_CATEGORIES, category.name)

        }
        return category
    }

    private fun convertAsanasDtoToModelList(
        response: Response<AsanasDto>,
        category: AsanaCategory
    ): List<Asana> {
        return _asanasDtoMapper.fromDataObjectToModel(response.body()!!)
            .onEach { asana -> asana.category = category }
    }

    private suspend fun saveAsanasToDb(
        asanaEntities: List<AsanaEntity>,
        lessonAllId: Int
    ) {
        _asanaDao.insertAsanas(asanaEntities)

        addAsanasToDefaultLessonAll(asanaEntities, lessonAllId)
    }

    private suspend fun saveDefaultLessonToDb(
        asanaEntities: List<AsanaEntity>,
        lessonName: String
    ): Long {
        return _lessonRepo.saveLesson(
            Lesson(
                0, lessonName,
                Date(), Date(),
                convertAsanaEntityToModel(asanaEntities),
                LessonType.DEFAULT
            )
        )
    }

    private suspend fun addAsanasToDefaultLessonAll(
        asanaEntities: List<AsanaEntity>,
        lessonAllId: Int
    ) {
        _lessonRepo.addAsanasToLesson(
            convertAsanaEntityToModel(asanaEntities),
            lessonAllId.toLong()
        )
    }

}