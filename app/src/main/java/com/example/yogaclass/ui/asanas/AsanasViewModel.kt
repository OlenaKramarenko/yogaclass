package com.example.yogaclass.ui.asanas

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.yogaclass.AppConstants.Asanas.TITLE_ALL
import com.example.yogaclass.AppConstants.Asanas.TITLE_BEGINNER_YOGA_SEQUENCE
import com.example.yogaclass.AppConstants.Asanas.TITLE_ESSENTIAL_YOGA_POSES
import com.example.yogaclass.AppConstants.Asanas.TITLE_LONG_AND_LEAN_FULL_BODY
import com.example.yogaclass.enums.AsanaCategory
import com.example.yogaclass.domain.repository.AsanaRepository
import com.example.yogaclass.model.Asana
import com.example.yogaclass.ui.base.BaseFilterViewModel
import com.example.yogaclass.ui.shared.model.FilterItem

class AsanasViewModel (private val _asanaRepo: AsanaRepository
) : BaseFilterViewModel() {

     var defaultAsanaCategory: AsanaCategory = AsanaCategory.ALL

    var asanaCategory = MutableLiveData<AsanaCategory>()

    private var _listFilterItems = MutableLiveData<List<FilterItem>>()
    override val listFilterItems: LiveData<List<FilterItem>>
        get() = _listFilterItems


    private val _loadingState = MutableLiveData<Boolean>()
    val loadingState: LiveData<Boolean>
        get() = _loadingState

    val asanasState: LiveData<List<Asana>> = Transformations.switchMap(asanaCategory) { category -> _asanaRepo.getLiveDataAsanasBy(category) }

    fun initFilters(){
        if(_listFilterItems.value == null) {
            _listFilterItems.postValue(
                listOf<FilterItem>(
                    FilterItem(AsanaCategory.ALL.filterKey, TITLE_ALL, true),
                    FilterItem(
                        AsanaCategory.ESSENTIAL_YOGA_POSES.filterKey,
                        TITLE_ESSENTIAL_YOGA_POSES,
                        false
                    ),
                    FilterItem(
                        AsanaCategory.BEGINNER_YOGA_SEQUENCE.filterKey,
                        TITLE_BEGINNER_YOGA_SEQUENCE,
                        false
                    ),
                    FilterItem(
                        AsanaCategory.LONG_AND_LEAN_FULL_BODY.filterKey,
                        TITLE_LONG_AND_LEAN_FULL_BODY,
                        false
                    )
                )
            )

            setCategory(defaultAsanaCategory)
        } else {
            _listFilterItems.postValue(_listFilterItems.value)
        }
    }

    fun setCategory(category: AsanaCategory) {
        asanaCategory.postValue(category)
    }

    fun updateFilters(selectedFilterId: Int){
        val filterItems = _listFilterItems.value
        filterItems?.forEach{ filterItem -> filterItem.selected = filterItem.id == selectedFilterId }
        _listFilterItems.postValue(filterItems)
    }
}