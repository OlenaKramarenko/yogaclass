package com.example.yogaclass.ui.shared.ext

import android.widget.ImageView
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.yogaclass.R
import com.example.yogaclass.ui.shared.lib.glide.GlideApp
import com.example.yogaclass.ui.shared.listener.GlideRequestListener
import timber.log.Timber

fun ImageView.load(imageUrl: String) {
    Glide
        .with(this)
        .load(imageUrl)
        .into(this)
}

fun ImageView.loadWithProgress(imageUrl: String, progress: ProgressBar, action: (() -> Unit)? = null) {
    progress.visible()

    val requestOptions = RequestOptions.placeholderOf(R.drawable.ic_diamond)
        .dontTransform()

    GlideApp
        .with(this)
        .load(imageUrl)
        .apply(requestOptions)
        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        .listener(GlideRequestListener(progress, action))
        .into(this)
}

