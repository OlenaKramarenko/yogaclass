package com.example.yogaclass.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.yogaclass.AppConstants
import com.example.yogaclass.dal.prefs.SharedPreferenceManager
import com.example.yogaclass.enums.AsanaCategory
import com.example.yogaclass.domain.repository.AsanaRepository
import com.example.yogaclass.network.util.NetworkResult
import com.example.yogaclass.ui.base.BaseViewModel
import com.example.yogaclass.ui.shared.enums.DataState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class MainViewModel(private val _asanaRepo: AsanaRepository,
                    private val _sharedPref: SharedPreferenceManager
) : BaseViewModel() {

    private val _loadingState = MutableLiveData<DataState>()
    val loadingState: LiveData<DataState>
        get() = _loadingState


    private val _asanasState = MutableLiveData<NetworkResult<List<AsanaCategory>>>()
    val asanasState: LiveData<NetworkResult<List<AsanaCategory>>>
        get() = _asanasState

    private val jobDefaultLessons = SupervisorJob()
    private val _bgScope = CoroutineScope(Dispatchers.IO + jobDefaultLessons)
    fun requestDataFromNetwork(){
        if(defaultLessonsNotExist())
            _bgScope.launch {
                changeLoadingState(DataState.LOADING)
                val result = _asanaRepo.getAllAsanas()

                _asanasState.postValue(result)
            }
    }

    var result: NetworkResult<List<AsanaCategory>>? = null

    fun defaultLessonsNotExist(): Boolean{
        return _sharedPref.getStringSet(AppConstants.KEY_SAVED_CATEGORIES)?.isEmpty() == true ||
                _sharedPref.getStringSet(AppConstants.KEY_SAVED_CATEGORIES)?.size != 3
    }

    fun changeLoadingState(loadingState: DataState){
        _loadingState.postValue(loadingState)
    }
}