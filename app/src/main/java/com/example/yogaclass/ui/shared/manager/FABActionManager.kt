package com.example.yogaclass.ui.shared.manager

import android.content.Context
import android.view.View
import com.example.yogaclass.R
import com.example.yogaclass.ui.shared.ext.invisible
import com.example.yogaclass.ui.shared.ext.visible
import com.google.android.material.floatingactionbutton.FloatingActionButton

class FABActionManager(
    private val _context: Context,
    private val _fabMain: FloatingActionButton,
    private val _actionCheckActionable: (() -> Boolean)? = null,
    private val _openIcon: Int = 0,
    private val _closeIcon: Int = 0,
    private val _actionMain: (() -> Unit)? = null,
    private val _viewAction1 : View? = null,
    private val _action1: (() -> Unit)? = null,
    private val _viewAction2 : View? = null,
    private val _action2: (() -> Unit)? = null,
    private val _viewAction3 : View? = null,
    private val _action3: (() -> Unit)? = null,
    private val _viewAction4 : View? = null,
    private val _action4: (() -> Unit)? = null,
    private val _viewAction5 : View? = null,
    private val _action5: (() -> Unit)? = null
    ) {

    private var _isFABActionable: Boolean = true
    var isFABActionable: Boolean
        get() = _isFABActionable
        set(value){
            _isFABActionable = value
        }

    init {
        setFABClickListeners()

        if(_viewAction1 == null &&
                _viewAction2 == null &&
                _viewAction3 == null &&
                _viewAction4 == null &&
                _viewAction5 == null) _isFABActionable = false
    }

    private fun setFABClickListeners(){
        _fabMain.setOnClickListener {
            isFABActionable = _actionCheckActionable?.invoke() ?: true

            if(!isFABActionable) {
                _actionMain?.invoke()
                return@setOnClickListener
            }

            if(!isFABOpen){
                showFABMenu();
            }else{
                closeFABMenu();
            }
        }

        _viewAction1?.setOnClickListener {
            _action1?.invoke()
        }

        _viewAction2?.setOnClickListener {
            _action2?.invoke()
        }

        _viewAction3?.setOnClickListener {
            _action3?.invoke()
        }

        _viewAction4?.setOnClickListener {
            _action4?.invoke()
        }

        _viewAction5?.setOnClickListener {
            _action5?.invoke()
        }
    }

    var isFABOpen: Boolean = false

    private fun showFABMenu(){
        isFABOpen=true
        _fabMain.setImageResource(_openIcon)
        _viewAction1?.visible()
        _viewAction1?.animate()?.translationY( - (_context.resources.getDimension(R.dimen.fab_action_trans_xs)))
        _viewAction2?.visible()
        _viewAction2?.animate()?.translationY( - (_context.resources.getDimension(R.dimen.fab_action_trans_sm)))
        _viewAction3?.visible()
        _viewAction3?.animate()?.translationY( - (_context.resources.getDimension(R.dimen.fab_action_trans_md)))
        _viewAction4?.visible()
        _viewAction4?.animate()?.translationY( - (_context.resources.getDimension(R.dimen.fab_action_trans_lg)))
        _viewAction5?.visible()
        _viewAction5?.animate()?.translationY( - (_context.resources.getDimension(R.dimen.fab_action_trans_xl)))
    }

    fun closeFABMenu() {
        if(!_isFABActionable) return

        isFABOpen = false
        _fabMain.setImageResource(_closeIcon)
        _viewAction1?.animate()?.translationY(0.toFloat())
        _viewAction1?.invisible()
        _viewAction2?.animate()?.translationY(0.toFloat())
        _viewAction2?.invisible()
        _viewAction3?.animate()?.translationY(0.toFloat())
        _viewAction3?.invisible()
        _viewAction4?.animate()?.translationY(0.toFloat())
        _viewAction4?.invisible()
        _viewAction5?.animate()?.translationY(0.toFloat())
        _viewAction5?.invisible()
    }

}