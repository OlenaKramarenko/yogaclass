package com.example.yogaclass.ui.settings

import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.example.yogaclass.AppConstants.TITLE_SETTINGS
import com.example.yogaclass.BuildConfig
import com.example.yogaclass.R
import com.example.yogaclass.domain.manager.settings.SettingsManager
import com.example.yogaclass.ui.main.MainActivity
import org.koin.android.ext.android.inject

class SettingsFragment : PreferenceFragmentCompat() {

    private val _settingsManager: SettingsManager by inject()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addPreferencesFromResource(R.xml.settings)

        initPreferences()
    }

    override fun onResume() {
        super.onResume()

        (activity as MainActivity).title = TITLE_SETTINGS
    }

    private fun initPreferences(){
        val appTheme: Preference? = findPreference(resources.getString(R.string.settings_app_theme_key))
        appTheme?.summary = _settingsManager.getAppThemeName(_settingsManager.getAppThemeId() ?: "")
        appTheme?.setOnPreferenceChangeListener { _, newValue ->
            appTheme.summary = _settingsManager.getAppThemeName(newValue as String)
            activity?.recreate()
            true
        }

        val appVersion = findPreference(resources.getString(R.string.settings_about_key))
        appVersion?.summary = String.format(
            getString(R.string.settings_app_version_description),
            "${BuildConfig.VERSION_NAME}.${BuildConfig.VERSION_CODE}"
        )

        val workOutTimeForRest: Preference? = findPreference(resources.getString(R.string.settings_time_for_rest_key))
        workOutTimeForRest?.summary = _settingsManager.getSelectedTimeForRest(_settingsManager.getIdOfSelectedTimeForRest() ?: "")
        workOutTimeForRest?.setOnPreferenceChangeListener { _, newValue ->
            workOutTimeForRest.summary = _settingsManager.getSelectedTimeForRest(newValue as String)
            true
        }
    }
}