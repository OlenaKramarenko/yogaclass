package com.example.yogaclass.ui.shared.enums

enum class DataState {
    RESULT,
    LOADING,
    NO_DATA,
    ERROR
}