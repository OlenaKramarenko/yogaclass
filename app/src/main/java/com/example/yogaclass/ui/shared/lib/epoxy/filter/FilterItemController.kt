package com.example.yogaclass.ui.shared.lib.epoxy.filter

import com.airbnb.epoxy.TypedEpoxyController
import com.example.yogaclass.ui.shared.model.FilterItem
import com.example.yogaclass.ui.shared.enums.FilterItemType


class FilterItemController(
    var func: (selectedItem: Int) -> Unit,
    var funcMinutes: ((newVal: Int) -> Unit)? = null,
    var funcSeconds: ((newVal: Int) -> Unit)? = null) : TypedEpoxyController<List<FilterItem>>(){

    override fun buildModels(filterItems: List<FilterItem>?) {
        filterItems?.forEach { filterItem ->
            when(filterItem.filterType) {
                FilterItemType.TEXT ->
                    FilterItemModel_()
                            .id(filterItem.id)
                            .title(filterItem.title)
                            .selected(filterItem.selected)
                            .action {
                                func(filterItem.id)
                            }
                            .addTo(this)
                FilterItemType.TEXT_TIME ->
                    FilterTimeItemModel_()
                        .id(filterItem.id)
                        .title(filterItem.title)
                        .selected(filterItem.selected)
                        .filterSelectedAction {
                            func(filterItem.id)
                        }
                        .minSelectedAction{newVal: Int ->
                            funcMinutes?.invoke(newVal)
                        }
                        .secSelectedAction{newVal: Int ->
                            funcSeconds?.invoke(newVal)
                        }
                        .minutes(filterItem.minutes)
                        .seconds(filterItem.seconds)
                        .addTo(this)
            }
        }
    }
}