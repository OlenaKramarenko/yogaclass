package com.example.yogaclass.ui.main

import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.example.yogaclass.R
import com.example.yogaclass.enums.AsanaCategory
import com.example.yogaclass.network.util.NetworkResult
import com.example.yogaclass.ui.base.BaseActivity
import com.example.yogaclass.ui.shared.enums.DataState
import com.example.yogaclass.ui.shared.ext.gone
import com.example.yogaclass.ui.shared.ext.visible
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.view_progressbar.*
import org.koin.androidx.viewmodel.ext.viewModel
import timber.log.Timber


class MainActivity : BaseActivity() {

    private val _mainViewModel: MainViewModel by viewModel()

    private lateinit var _appBarConfiguration: AppBarConfiguration

    private lateinit var _host: NavHostFragment

    lateinit var navController: NavController

    lateinit var bottomNavigation: BottomNavigationView

    var txtBackdropTitle: TextView? = null

    private val listDestinations: List<Int> = listOf( R.id.lessons_fragment, R.id.asanas_fragment, R.id.settings_dest )

    private var _firstLoading = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        _host = supportFragmentManager
            .findFragmentById(R.id.nav_main) as NavHostFragment? ?: return
        navController = _host.navController

        setupBottomNavMenu(navController)

        subscribeViewModel()

        if (_firstLoading) {
            _mainViewModel.requestDataFromNetwork()
            _firstLoading = false
        }
    }

    private fun setupBottomNavMenu(navController: NavController) {
        bottomNavigation = bnv
        bottomNavigation.setupWithNavController(navController)

        navController.addOnDestinationChangedListener{
                _, destination: NavDestination, _ ->

            if(!listDestinations.contains(destination.id)) bottomNavigation.gone()
            else bottomNavigation.visible()
        }
    }

    private fun subscribeViewModel() {
        subscribeLoading()
        subscribeAsanas()
    }

    private fun subscribeLoading() {
        _mainViewModel.loadingState.observe(this, Observer { loadingState ->
            Timber.d("--------------------- MainActivity.loadingState.observe $loadingState ---------------------------------------")
            if (loadingState == DataState.LOADING){
                navController.navigate(R.id.nav_splash)
            } else if(loadingState == DataState.RESULT) {
                navController.popBackStack(R.id.asanas_fragment, false)
            } else if(loadingState == DataState.ERROR || loadingState == DataState.NO_DATA){

                // TODO screen for error and no_data !!!!!!!
                v_progress.gone()
            }
        })
    }

    private var attempts: Int = 0
    private fun subscribeAsanas() {
        _mainViewModel.asanasState.observe(this,
            Observer<NetworkResult<List<AsanaCategory>>> { result ->
                if(!_mainViewModel.defaultLessonsNotExist())
                    _mainViewModel.changeLoadingState(DataState.RESULT)
                else if(attempts == 4){
                    _mainViewModel.changeLoadingState(DataState.NO_DATA)
                } else if(result.completedSuccessfully())
                    _mainViewModel.changeLoadingState(DataState.RESULT)
                else if(result.isConnectionError)
                    _mainViewModel.changeLoadingState(DataState.ERROR)
                else
                    attempts++
            })
    }

    override fun onSupportNavigateUp(): Boolean {
        // Allows NavigationUI to support proper up navigation or the drawer layout
        // drawer menu, depending on the situation
        return findNavController(R.id.nav_main).navigateUp(_appBarConfiguration)
    }

}
