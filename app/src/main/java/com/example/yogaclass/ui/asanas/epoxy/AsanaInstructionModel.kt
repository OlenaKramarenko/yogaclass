package com.example.yogaclass.ui.asanas.epoxy

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.yogaclass.R
import com.example.yogaclass.ui.shared.lib.epoxy.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.list_item_asana_instruction)
abstract class AsanaInstructionModel : EpoxyModelWithHolder<AsanaInstructionHolder>() {
    @EpoxyAttribute
    lateinit var text: String

    override fun bind(holder: AsanaInstructionHolder) {
        holder.txtText.text = text
    }
}

class AsanaInstructionHolder : KotlinEpoxyHolder() {
    val txtText by bind<TextView>(R.id.txt_text)
}