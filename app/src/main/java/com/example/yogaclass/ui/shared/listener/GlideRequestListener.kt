package com.example.yogaclass.ui.shared.listener

import android.graphics.drawable.Drawable
import android.widget.ProgressBar
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.yogaclass.ui.shared.ext.gone
import timber.log.Timber

class GlideRequestListener(var progressBar: ProgressBar, var action: (() -> Unit)? = null) : RequestListener<Drawable> {
    override fun onLoadFailed(
        e: GlideException?,
        model: Any?,
        target: Target<Drawable>?,
        isFirstResource: Boolean
    ): Boolean {
        progressBar.gone()

        Timber.e("---------------------  Error Message ${e?.message} ---------------------------------------")

        action?.invoke()

        return false
    }

    override fun onResourceReady(
        resource: Drawable?,
        model: Any?,
        target: Target<Drawable>?,
        dataSource: DataSource?,
        isFirstResource: Boolean
    ): Boolean {
        progressBar.gone()
        action?.invoke()

        return false
    }

}