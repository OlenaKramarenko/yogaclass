package com.example.yogaclass.ui.lessons.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.yogaclass.R
import com.example.yogaclass.enums.ClockState
import com.example.yogaclass.enums.ClockType
import com.example.yogaclass.ui.lessons.AsanaWorkOutViewModel
import com.example.yogaclass.ui.lessons.ClockViewModel
import com.example.yogaclass.ui.shared.ext.gone
import com.example.yogaclass.ui.shared.ext.invisible
import com.example.yogaclass.ui.shared.ext.loadWithProgress
import com.example.yogaclass.ui.shared.ext.visible

class LessonWorkOutAdapter(var context: Context,
                           var asanas: MutableList<AsanaWorkOutViewModel>,
                           var clockType: ClockType,
                           var state: ClockState,
                           var funcStartPauseTimer: () -> Unit): RecyclerView.Adapter<LessonWorkOutAdapter.LessonWorkOutViewHolder>() {

    private var _stateClockForRest: ClockState = ClockState.Stop

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LessonWorkOutViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_lesson_asana_workout, parent, false))

    override fun getItemCount() = asanas.count()

    override fun onBindViewHolder(holder: LessonWorkOutViewHolder, position: Int)
            = holder.bind(context, asanas[position], clockType, state, _stateClockForRest, funcStartPauseTimer)

    fun changeClockType(clockType: ClockType){
        this.clockType = clockType
        _stateClockForRest = ClockState.Stop

        notifyDataSetChanged()
    }

    fun changeClockState(state: ClockState){
        this.state = state
        notifyDataSetChanged()
    }

    fun setData(asanaWorkOuts: List<AsanaWorkOutViewModel>){
        for((index, asanaWorkOut) in asanaWorkOuts.withIndex()){
            asanas[index] = asanaWorkOut
        }
        notifyDataSetChanged()
    }

    fun updateClockForCurrentItem(clockViewModel: ClockViewModel, currentItem: Int){
        asanas[currentItem].secLeft = clockViewModel.secondsLeft
        asanas[currentItem].secPassed = clockViewModel.secondsPassed
        asanas[currentItem].secFullTime = clockViewModel.secondsFullTime

        notifyDataSetChanged()
    }

    fun updateRestClockForCurrentItem(clockViewModel: ClockViewModel, currentItem: Int){
        asanas[currentItem].secLeftForRest = clockViewModel.secondsLeft.toInt()
        asanas[currentItem].secRestFullTime = clockViewModel.secondsFullTime.toInt()

        notifyDataSetChanged()
    }

    fun changeClockForRestState(state: ClockState){
        this._stateClockForRest = state
        notifyDataSetChanged()
    }

    class LessonWorkOutViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        private var _ivAsana: ImageView = view.findViewById(R.id.iv_asana)
        private var _txtTitle: TextView = view.findViewById(R.id.txt_title)
        private var _progressbar: ProgressBar = view.findViewById(R.id.progress_for_image)
        private var _progressTimer: ProgressBar = view.findViewById(R.id.progress_timer)
        private var _timerWrapper: ConstraintLayout = view.findViewById(R.id.con_timer_wrapper)
        private var _txtTimer: TextView = view.findViewById(R.id.txt_time)
        private var _txtLabelTapStartPause: TextView = view.findViewById(R.id.txt_label_tap_start_pause)
        private var _timerForRestWrapper: ConstraintLayout = view.findViewById(R.id.con_timer_for_rest)
        private var _txtLabelTimerForRest: TextView = view.findViewById(R.id.txt_lable_time_for_rest)
        private var _txtLabelTimerForRestSeconds: TextView = view.findViewById(R.id.txt_lable_time_for_rest_seconds)
        private var _txtTimerForRest: TextView = view.findViewById(R.id.txt_time_for_rest)

        fun bind(context: Context,
                 asanaItem: AsanaWorkOutViewModel,
                 clockType: ClockType, state: ClockState, clockForRestState: ClockState,
                 funcStartPauseTimer: () -> Unit) {

            if(asanaItem.imageUrl.isBlank())
                _progressbar.visible()
            else
                _ivAsana.loadWithProgress(asanaItem.imageUrl, _progressbar)

            _txtTitle.text = asanaItem.title

            if(clockType == ClockType.TIMER){
                _progressTimer.visible()
                _progressTimer.max = asanaItem.secFullTime.toInt()
                _progressTimer.secondaryProgress = asanaItem.secFullTime.toInt()
                _progressTimer.progress = asanaItem.secLeft.toInt()
                _txtTimer.text = asanaItem.secLeft.toString()
            }
            else if(clockType == ClockType.STOPWATCH) {
                _progressTimer.invisible()
                _txtTimer.text = asanaItem.secPassed.toString()
            }

            changeStartPauseLabel(context, state)

            _timerWrapper.setOnClickListener {
                funcStartPauseTimer()
            }

            manageClockForRest(context, clockForRestState, asanaItem)
        }

        private fun changeStartPauseLabel(context: Context, state: ClockState){
            if(state == ClockState.Stop || state == ClockState.OnPause){
                _txtLabelTapStartPause.visible()
                _txtLabelTapStartPause.text = context.getString(R.string.workout_label_tap_start)
            } else if(state == ClockState.Running){
                _txtLabelTapStartPause.visible()
                _txtLabelTapStartPause.text = context.getString(R.string.workout_label_tap_pause)
            }
        }

        private fun manageClockForRest(context: Context, clockForRestState: ClockState, asanaItem: AsanaWorkOutViewModel){
            if(clockForRestState == ClockState.Stop){
                _timerForRestWrapper.gone()
            } else if(clockForRestState == ClockState.Running){
                _timerForRestWrapper.visible()
                _txtLabelTimerForRest.text = context.getString(R.string.workout_label_time_for_rest)
                _txtLabelTimerForRestSeconds.text = if(asanaItem.secLeftForRest > 1) context.getString(R.string.workout_label_time_for_rest_seconds)
                                                    else context.getString(R.string.workout_label_time_for_rest_seconds)
                                                            .substring(0, context.getString(R.string.workout_label_time_for_rest_seconds).length-1)

                _txtTimerForRest.text = asanaItem.secLeftForRest.toString()
            }
        }
    }
}