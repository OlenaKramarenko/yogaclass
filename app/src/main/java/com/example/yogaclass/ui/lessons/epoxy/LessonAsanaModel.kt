package com.example.yogaclass.ui.lessons.epoxy

import android.widget.ImageView
import android.widget.ProgressBar
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.yogaclass.R
import com.example.yogaclass.ui.shared.ext.loadWithProgress
import com.example.yogaclass.ui.shared.lib.epoxy.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.list_item_lesson_asana)
abstract class LessonAsanaModel : EpoxyModelWithHolder<LessonAsanaHolder>() {

    @EpoxyAttribute
    lateinit var previewUrl: String

    override fun bind(holder: LessonAsanaHolder) {
        holder.ivAsanaImage.loadWithProgress(previewUrl, holder.progressbar)
    }
}

class LessonAsanaHolder : KotlinEpoxyHolder() {
    val ivAsanaImage by bind<ImageView>(R.id.iv_asana)
    val progressbar by bind<ProgressBar>(R.id.v_progress)
}