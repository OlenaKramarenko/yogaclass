package com.example.yogaclass.ui.asanas.epoxy

import com.airbnb.epoxy.TypedEpoxyController
import com.example.yogaclass.model.Asana

class AsanasController(val func: (Asana) -> Unit) : TypedEpoxyController<List<Asana>>() {
    override fun buildModels(asanaList: List<Asana>?) {
        asanaList?.forEach {
            asanasList {
                id(it.asanaId)
                asanaId(it.asanaId)
                title(it.title)
                selected(it.selected)
                previewUrl(it.previewUrl ?: "")
                action { func(it) }
            }
        }
    }
}