package com.example.yogaclass.ui.shared.model

import com.example.yogaclass.ui.shared.enums.FilterItemType

data class FilterItem (
    var id: Int,
    val title: String,
    var selected: Boolean = false,
    var filterType: FilterItemType = FilterItemType.TEXT,

    var minutes: Int = 0,
    var seconds: Int = 0
)
