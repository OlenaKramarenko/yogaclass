package com.example.yogaclass.ui.lessons

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.yogaclass.AppConstants
import com.example.yogaclass.domain.event.TimeProgressEventArgs
import com.example.yogaclass.domain.event.TimeStateEventArgs
import com.example.yogaclass.domain.manager.settings.SettingsManager
import com.example.yogaclass.domain.repository.AsanaRepository
import com.example.yogaclass.domain.repository.LessonRepository
import com.example.yogaclass.domain.service.DefaultClockService
import com.example.yogaclass.domain.service.contracts.ClockService
import com.example.yogaclass.enums.ClockState
import com.example.yogaclass.enums.ClockType
import com.example.yogaclass.model.Asana
import com.example.yogaclass.ui.base.BaseFilterViewModel
import com.example.yogaclass.ui.shared.enums.FilterItemType
import com.example.yogaclass.ui.shared.model.FilterItem
import com.example.yogaclass.util.DateTimeUtil
import com.example.yogaclass.util.DateTimeUtil.getSecondsFromMillis
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*

class LessonViewModel(
    private val lessonRepo: LessonRepository,
    private val asanaRepo: AsanaRepository,
    _settingsManager: SettingsManager
) : BaseFilterViewModel() {

    private val defaultSeconds: Int = 20

    private var selectedClockType: ClockType = ClockType.STOPWATCH
    private var minutes: Long = 0
    private var seconds: Long = 0
    private val duration: Long
        get() = DateTimeUtil.getSecondsFromMinutes(minutes) + seconds

    private val secondsTimeForRest: Long =
        _settingsManager.getSecondsForRest(_settingsManager.getIdOfSelectedTimeForRest() ?: "")
            .toLong()

    private var _listFilterItems = MutableLiveData<List<FilterItem>>()
    override val listFilterItems: LiveData<List<FilterItem>>
        get() = _listFilterItems

    fun initFilters() {
        _listFilterItems.postValue(
            listOf<FilterItem>(
                FilterItem(
                    ClockType.STOPWATCH.filterKey,
                    AppConstants.Lesson.TITLE_FILTER_MANUALLY,
                    true
                ),
                FilterItem(
                    ClockType.TIMER.filterKey,
                    AppConstants.Lesson.TITLE_FILTER_TIMER,
                    false,
                    FilterItemType.TEXT_TIME,
                    minutes.toInt(),
                    defaultSeconds
                )
            )
        )

        _clockService.changeClockType(selectedClockType, duration)
    }

    fun updateFilters(selectedFilterId: Int) {
        val filterItems = _listFilterItems.value
        filterItems?.forEach { filterItem ->
            filterItem.selected = filterItem.id == selectedFilterId
        }
        Timber.d("--------------------- LessonViewModel.updateFilters $selectedFilterId ---------------------------------------")
        _listFilterItems.postValue(filterItems)
    }

    private val _loadingState = MutableLiveData<Boolean>()
    val loadingState: LiveData<Boolean>
        get() = _loadingState

    private var _lessonId = MutableLiveData<Int>()

    val asanaListState: LiveData<List<Asana>> =
        Transformations.switchMap(_lessonId) { lessonId -> lessonRepo.getLiveDataAsanasBy(lessonId) }

    private val jobLesson = SupervisorJob()
    private val _bgScope = CoroutineScope(Dispatchers.IO + jobLesson)
    private suspend fun updateLessonLastUsageDate(lessonId: Int) {
        lessonRepo.updateLessonLastUsageDate(lessonId, Date())
    }

    fun getLesson(lessonId: Int) {
        _bgScope.launch {
            updateLessonLastUsageDate(lessonId)
        }
        _lessonId.postValue(lessonId)
    }

    fun getAsanaWorkOutList(asanas: List<Asana>): MutableList<AsanaWorkOutViewModel> {
        return asanas.map { asana: Asana -> this.getAsanaWorkOut(asana) }.toMutableList()
    }

    private fun getAsanaWorkOut(asana: Asana): AsanaWorkOutViewModel = AsanaWorkOutViewModel(
        asana.asanaId, asana.title,
        asana.imageUrl ?: "", duration,
        asana.url
    )

    suspend fun getFullAsanaWorkOut(asana: AsanaWorkOutViewModel): AsanaWorkOutViewModel {
        val result = asanaRepo.getFullAsana(asana.url)
        return if (result.data != null)
            getAsanaWorkOut(result.data!!)
        else
            asana
    }

    private val _clockViewModel = MutableLiveData<ClockViewModel>()
    val clockViewModel: LiveData<ClockViewModel>
        get() = _clockViewModel

    private val _clockState = MutableLiveData<ClockState>()
    val clockState: LiveData<ClockState>
        get() = _clockState

    private lateinit var _clockService: ClockService
    fun createClockService() {
        _clockService = DefaultClockService(::timerStateChanged, ::timerProgressChanged)
    }

    private fun timerStateChanged(timeStateEvent: TimeStateEventArgs) {
        if (timeStateEvent.state == ClockState.Stop)
            resetClockToInitial()
        else
            _clockState.postValue(timeStateEvent.state)
    }

    private fun timerProgressChanged(timeProgressEvent: TimeProgressEventArgs) {
        setClockViewModel(
            getSecondsFromMillis(timeProgressEvent.millisPassed),
            getSecondsFromMillis(timeProgressEvent.millisLeft)
        )
    }

    private val _clockForRestViewModel = MutableLiveData<ClockViewModel>()
    val clockForRestViewModel: LiveData<ClockViewModel>
        get() = _clockForRestViewModel

    private val _clockForRestState = MutableLiveData<ClockState>()
    val clockForRestState: LiveData<ClockState>
        get() = _clockForRestState

    private lateinit var _clockServiceForRest: ClockService
    fun createClockServiceForRest() {
        _clockServiceForRest = DefaultClockService(
            ::timerForRestStateChanged, ::timerForRestProgressChanged,
            secondsTimeForRest, ClockType.TIMER
        )
    }

    private fun timerForRestStateChanged(timeStateEvent: TimeStateEventArgs) {
        _clockForRestState.postValue(timeStateEvent.state)
    }

    fun resetTimerForRestToInitial() {
        _clockServiceForRest.stop()

        _clockForRestState.postValue(ClockState.Stop)
        setClockForRestViewModel(secondsTimeForRest)
    }

    private fun timerForRestProgressChanged(timeProgressEvent: TimeProgressEventArgs) {
        setClockForRestViewModel(getSecondsFromMillis(timeProgressEvent.millisLeft))
    }

    private fun setClockForRestViewModel(newSecLeft: Long) {
        val clock: ClockViewModel?
        if (_clockForRestViewModel.value == null)
            clock = ClockViewModel(
                secondsPassed = 0,
                secondsLeft = secondsTimeForRest,
                secondsFullTime = secondsTimeForRest
            )
        else
            clock = _clockForRestViewModel.value?.apply {
                secondsLeft = newSecLeft
                secondsFullTime = secondsTimeForRest
            }
        _clockForRestViewModel.postValue(clock)
    }

    fun changeSelectedClockType(selectedClockType: ClockType) {
        this.selectedClockType = selectedClockType

        resetClockToInitial()

        _clockService.changeClockType(selectedClockType, duration)
        _clockState.postValue(ClockState.Stop)

        resetTimerForRestToInitial()
    }

    fun getSelectedClockType() = selectedClockType

    fun changeClockTime(minNewValue: Long? = null, secNewValue: Long? = null) {
        if (minNewValue != null)
            minutes = minNewValue
        if (secNewValue != null)
            seconds = secNewValue

        stopClock()
    }

    private fun setClockViewModel(newSecPassed: Long, newSecLeft: Long) {
        val clock: ClockViewModel?
        if (_clockViewModel.value == null)
            clock = ClockViewModel(
                secondsPassed = 0,
                secondsLeft = duration,
                secondsFullTime = duration
            )
        else
            clock = _clockViewModel.value?.apply {
                secondsPassed = newSecPassed
                secondsLeft = newSecLeft
                secondsFullTime = duration
            }
        _clockViewModel.postValue(clock)
    }

    fun startPauseClock() {
        if (_clockServiceForRest.state != ClockState.Stop) {
            resetTimerForRestToInitial()
        }

        Timber.d("--------------------- LessonViewModel.startPauseClock ${_clockService.state} --------------------------------")
        when {
            _clockService.state == ClockState.Stop -> _clockService.start()
            _clockService.state == ClockState.OnPause -> _clockService.resume()
            _clockService.state != ClockState.OnPause -> _clockService.pause()
        }
    }

    fun showNextAsana() {
        stopClock()

        resetClockToInitial()

        _clockServiceForRest.start()
    }

    fun startClockService() {
        _clockService.start()
    }

    fun stopClock() {
        if (_clockService.state != ClockState.Stop) {
            _clockService.stop()
            _clockState.postValue(ClockState.Stop)
        }
        if (_clockServiceForRest.state != ClockState.Stop) {
            resetTimerForRestToInitial()
        }
        _clockService.setStartCountdownTimeSeconds(duration)
        setClockViewModel(0, duration)
    }

    private fun resetClockToInitial() {
        setTimeAsDefault()
        _clockState.postValue(ClockState.Stop)
        setClockViewModel(0, duration)
    }

    private fun setTimeAsDefault() {
        seconds = if (selectedClockType == ClockType.TIMER) duration else 0
    }
}