package com.example.yogaclass.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.yogaclass.AppConstants.Settings.THEME_ID_BASIL
import com.example.yogaclass.AppConstants.Settings.THEME_ID_LAVENDER
import com.example.yogaclass.AppConstants.Settings.THEME_ID_PURPLE
import com.example.yogaclass.AppConstants.Settings.THEME_ID_RED
import com.example.yogaclass.R
import com.example.yogaclass.domain.manager.settings.SettingsManager
import org.koin.android.ext.android.inject
import timber.log.Timber

abstract class BaseActivity : AppCompatActivity(){

    private val _settingsManager: SettingsManager by inject()

    var appThemeId: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        setAppTheme()

        super.onCreate(savedInstanceState)

    }

    override fun onResume() {
        super.onResume()

        if (appThemeId != _settingsManager.getAppThemeId()){
            recreate()
        }
    }

    private fun setAppTheme(){
        appThemeId = _settingsManager.getAppThemeId()
        Timber.d("---------------------  BaseActivity.setAppTheme $appThemeId ---------------------------------------")
        when(appThemeId){
            THEME_ID_LAVENDER -> setTheme(R.style.AppTheme_Lavender)
            THEME_ID_PURPLE -> setTheme(R.style.AppTheme_Purple)
            THEME_ID_BASIL -> setTheme(R.style.AppTheme_Basil)
            THEME_ID_RED -> setTheme(R.style.AppTheme_Red)
            else -> setTheme(R.style.AppTheme_Peach)
        }
    }
}