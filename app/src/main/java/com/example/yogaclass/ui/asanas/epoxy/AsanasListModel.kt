package com.example.yogaclass.ui.asanas.epoxy

import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.yogaclass.R
import com.example.yogaclass.ui.shared.ext.loadWithProgress
import com.example.yogaclass.ui.shared.lib.epoxy.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.list_item_asana)
abstract class AsanasListModel : EpoxyModelWithHolder<AsanaHolder>() {

    @EpoxyAttribute
    lateinit var asanaId: String

    @EpoxyAttribute
    lateinit var title: String

    @EpoxyAttribute
    var selected: Boolean = false

    @EpoxyAttribute
    lateinit var previewUrl: String

    @EpoxyAttribute
    lateinit var action: () -> Unit

    override fun bind(holder: AsanaHolder) {
        holder.txtDescription.apply {
            text = title
        }
        holder.ivImage.apply {
            loadWithProgress(previewUrl, holder.progressbar)
        }

        holder.conItemWrapper.setOnClickListener {
            action()
        }
    }
}

class AsanaHolder : KotlinEpoxyHolder() {
    val ivImage by bind<ImageView>(R.id.iv_asana)
    val txtDescription by bind<TextView>(R.id.txt_description)
    val conItemWrapper by bind<RelativeLayout>(R.id.coordl_assans_wrapper)
    val progressbar by bind<ProgressBar>(R.id.v_progress)
}