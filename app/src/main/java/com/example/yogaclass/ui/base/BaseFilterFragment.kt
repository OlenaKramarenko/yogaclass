package com.example.yogaclass.ui.base

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyRecyclerView
import com.example.yogaclass.AppConstants
import com.example.yogaclass.R
import com.example.yogaclass.ui.shared.ext.animateFadeIn
import com.example.yogaclass.ui.shared.ext.animateFadeOut
import com.example.yogaclass.ui.shared.lib.epoxy.filter.FilterItemController
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.fragment_asanas.*
import kotlinx.android.synthetic.main.view_backdrop_asanas.*

abstract class BaseFilterFragment : BaseFragment(), AppBarLayout.OnOffsetChangedListener {
    var filterController: FilterItemController? = null

    var maxScrollSize: Int = 0
    var isToolbarCollapsed = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (firstLoading) {
            val appbar = view.findViewById<AppBarLayout>(R.id.app_bar)
            appbar.addOnOffsetChangedListener(this)
            firstLoading = false
        }
    }

    open fun initFilters() {
        filterController = FilterItemController(::updateContentBy)

        rv_filter.layoutManager = LinearLayoutManager(mainActivity)
        filterController?.let {
            rv_filter.setController(it)
        }
    }

    abstract fun updateContentBy(selectedFilterItem: Int)

    // for disappearing filter-menu when toolbar is collapsed
    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        if (maxScrollSize == 0)
            maxScrollSize = appBarLayout!!.totalScrollRange

        val currentScrollPercentage = (Math.abs(verticalOffset)) * 100 / maxScrollSize

        if (currentScrollPercentage >= AppConstants.PERCENTAGE_TO_SHOW_CONTENT_IN_TOOLBAR) {
            if (!isToolbarCollapsed) {
                isToolbarCollapsed = true

                lin_filter.animateFadeOut(200.toLong())
            }
        }

        if (currentScrollPercentage < AppConstants.PERCENTAGE_TO_SHOW_CONTENT_IN_TOOLBAR) {
            if (isToolbarCollapsed) {
                isToolbarCollapsed = false

                lin_filter.animateFadeIn(500.toLong())
            }
        }
    }

    override fun onResume() {
        super.onResume()

        initFilters()
    }
}