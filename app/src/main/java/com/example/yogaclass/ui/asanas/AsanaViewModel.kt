package com.example.yogaclass.ui.asanas

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.yogaclass.domain.repository.AsanaRepository
import com.example.yogaclass.model.Asana
import com.example.yogaclass.network.util.NetworkResult
import com.example.yogaclass.ui.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class AsanaViewModel(private val _asanaRepo: AsanaRepository) : BaseViewModel() {

    private val jobAssanas = SupervisorJob()
    private val bgScope = CoroutineScope(Dispatchers.IO + jobAssanas)

    private val _asanaState = MutableLiveData<NetworkResult<Asana>>()
    val asanaState: LiveData<NetworkResult<Asana>>
        get() = _asanaState

    fun getAsana(url: String) {
        bgScope.launch {
            val asana = _asanaRepo.getFullAsana(url)
            _asanaState.postValue(asana)
        }
    }

}