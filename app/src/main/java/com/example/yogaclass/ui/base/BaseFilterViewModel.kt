package com.example.yogaclass.ui.base

import androidx.lifecycle.LiveData
import com.example.yogaclass.ui.shared.model.FilterItem

abstract class BaseFilterViewModel: BaseViewModel() {
    abstract val listFilterItems : LiveData<List<FilterItem>>
}