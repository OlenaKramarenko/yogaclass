package com.example.yogaclass.ui.shared.enums

enum class FilterItemType {
    TEXT,
    TEXT_TIME
}