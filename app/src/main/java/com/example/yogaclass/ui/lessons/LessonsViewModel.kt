package com.example.yogaclass.ui.lessons

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.yogaclass.AppConstants.Lessons.TITLE_FILTER_BY_CREATION_DATE
import com.example.yogaclass.AppConstants.Lessons.TITLE_FILTER_BY_LAST_USAGE_DATE
import com.example.yogaclass.AppConstants.Lessons.TITLE_FILTER_BY_NAME
import com.example.yogaclass.enums.LessonsFilterType
import com.example.yogaclass.domain.repository.LessonRepository
import com.example.yogaclass.ui.shared.model.FilterItem
import com.example.yogaclass.model.Lesson
import com.example.yogaclass.ui.base.BaseFilterViewModel
import kotlinx.coroutines.*
import timber.log.Timber

class LessonsViewModel(
    private val _lessonRepo: LessonRepository
) : BaseFilterViewModel() {

    var selectedFilterType: LessonsFilterType = LessonsFilterType.NAME

     private var _listFilterItems = MutableLiveData<List<FilterItem>>()
    override val listFilterItems: LiveData<List<FilterItem>>
        get() = _listFilterItems

    fun initFilters(){
        if(_listFilterItems.value == null) {
            _listFilterItems.postValue(
                listOf<FilterItem>(
                    FilterItem(
                        LessonsFilterType.NAME.filterKey,
                        TITLE_FILTER_BY_NAME,
                        true
                    ),
                    FilterItem(
                        LessonsFilterType.CREATION_DATE.filterKey,
                        TITLE_FILTER_BY_CREATION_DATE,
                        false
                    ),
                    FilterItem(
                        LessonsFilterType.LAST_USAGE_DATE.filterKey,
                        TITLE_FILTER_BY_LAST_USAGE_DATE,
                        false
                    )
                )
            )
        }
        else {
            _listFilterItems.postValue(_listFilterItems.value)
        }
    }

    private val _loadingState = MutableLiveData<Boolean>()
    val loadingState: LiveData<Boolean>
        get() = _loadingState

    private val _lessonsState = MutableLiveData<List<Lesson>>()
    val lessonsState: LiveData<List<Lesson>>
        get() = _lessonsState

    private val jobLessons = SupervisorJob()
    private val _bgScope = CoroutineScope(Dispatchers.IO + jobLessons)

    fun getLessons(filterType: LessonsFilterType) {
        if (_loadingState.value == true) {
            _bgScope.coroutineContext.cancelChildren()
        }

        _loadingState.postValue(true)
        _bgScope.launch {
            val lessons = _lessonRepo.getLessonsByFilterType(filterType)
            _lessonsState.postValue(lessons)
            _loadingState.postValue(false)
        }
    }

    fun updateFilters(selectedFilterId: Int){
        val filterItems = _listFilterItems.value
        filterItems?.forEach{ filterItem -> filterItem.selected = filterItem.id == selectedFilterId }
        Timber.e("--------------------- LessonsViewModel.updateFilters $selectedFilterId ---------------------------------------")
        _listFilterItems.postValue(filterItems)
    }
}