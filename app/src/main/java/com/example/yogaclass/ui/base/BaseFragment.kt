package com.example.yogaclass.ui.base

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.postDelayed
import androidx.fragment.app.Fragment
import androidx.transition.TransitionInflater
import com.example.yogaclass.R
import com.example.yogaclass.ui.main.MainActivity

abstract class BaseFragment : Fragment() {

    val mainActivity: MainActivity?
        get() = activity as MainActivity

    abstract val fragmentLayoutId: Int

    var firstLoading = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        return inflater.inflate(fragmentLayoutId, container, false)
    }
}