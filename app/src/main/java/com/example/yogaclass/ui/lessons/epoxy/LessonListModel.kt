package com.example.yogaclass.ui.lessons.epoxy

import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.airbnb.epoxy.EpoxyRecyclerView
import com.example.yogaclass.R
import com.example.yogaclass.model.Asana
import com.example.yogaclass.ui.shared.lib.epoxy.KotlinEpoxyHolder
import com.example.yogaclass.ui.shared.lib.epoxy.withModels

@EpoxyModelClass(layout = R.layout.list_item_lessons)
abstract class LessonListModel : EpoxyModelWithHolder<LessonHolder>() {
    @EpoxyAttribute
    lateinit var title: String

    @EpoxyAttribute
    lateinit var asanas: List<Asana>

    @EpoxyAttribute
    lateinit var action: () -> Unit

    override fun bind(holder: LessonHolder) {
        holder.txtLessonTitle.text = title
        holder.rvAsanas.withModels {
            asanas.forEach {
                lessonAsana {
                    id(it.asanaId)
                    previewUrl(it.previewUrl ?: "")
                }
            }
        }

        holder.rvAsanas.setOnClickListener{
            action()
        }

        holder.conItemWrapper.setOnClickListener {
            action()
        }
        holder.ivOpenLesson.setOnClickListener {
            action()
        }
    }
}

class LessonHolder : KotlinEpoxyHolder() {
    val txtLessonTitle by bind<TextView>(R.id.txt_title)
    val rvAsanas by bind<EpoxyRecyclerView>(R.id.rv_asana_img)
    val conItemWrapper by bind<ConstraintLayout>(R.id.con_lesson_wrapper)
    val ivOpenLesson by bind<View>(R.id.iv_open_lesson)
}