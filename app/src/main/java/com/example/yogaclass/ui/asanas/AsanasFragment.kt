package com.example.yogaclass.ui.asanas

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyItemSpacingDecorator
import com.example.yogaclass.AppConstants.TAB_ASANAS
import com.example.yogaclass.R
import com.example.yogaclass.enums.AsanaCategory
import com.example.yogaclass.model.Asana
import com.example.yogaclass.ui.asanas.epoxy.AsanasController
import com.example.yogaclass.ui.base.BaseFilterFragment
import com.example.yogaclass.ui.shared.ext.gone
import com.example.yogaclass.ui.shared.ext.visible
import com.example.yogaclass.ui.shared.model.FilterItem
import kotlinx.android.synthetic.main.fragment_asanas.*
import kotlinx.android.synthetic.main.view_progressbar.*
import org.koin.androidx.viewmodel.ext.viewModel

class AsanasFragment : BaseFilterFragment() {

    private val asanasViewModel: AsanasViewModel by viewModel()

    override val fragmentLayoutId: Int = R.layout.fragment_asanas

    private lateinit var asanaController: AsanasController

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        subscribeViewModel()
    }

    private fun initView() {
        mainActivity?.txtBackdropTitle?.gone()

        tlb.title = TAB_ASANAS
    }

    override fun initFilters() {
        super.initFilters()

        asanasViewModel.initFilters()

        subscribeFilters()
    }

    private fun subscribeFilters() {
        asanasViewModel.listFilterItems.observe(this, Observer {
            setFilterData(it)
        })
    }

    private fun setFilterData(filters: List<FilterItem>) {
        filterController?.setData(filters)
    }

    override fun updateContentBy(selectedFilterItem: Int) {
        asanasViewModel.setCategory(AsanaCategory.getCategoryBy(selectedFilterItem))
        asanasViewModel.updateFilters(selectedFilterItem)
    }

    private fun subscribeViewModel() {
        subscribeLoading()
        subscribeAsanas()
    }

    private fun subscribeLoading() {
        asanasViewModel.loadingState.observe(this, Observer { isLoading ->
            if (isLoading) {
                v_progress.visible()
            } else {
                v_progress.gone()
            }
        })
    }

    private fun subscribeAsanas() {
        asanasViewModel.asanasState.observe(viewLifecycleOwner,
            Observer<List<Asana>> { asanas -> setAsanas(asanas) })
    }

    private fun setAsanas(asanas: List<Asana>?) {
        if (rv_assans.adapter == null) {
            asanaController = AsanasController(::navigateToAsana)

            rv_assans.apply {
                layoutManager = LinearLayoutManager(activity)
                adapter = asanaController.adapter
                addItemDecoration(EpoxyItemSpacingDecorator(resources.getDimension(R.dimen.app_space_md).toInt()))
            }
        }

        asanaController.setData(asanas)
    }

    private fun navigateToAsana(selectedAsana: Asana) {
        this.findNavController().navigate(
            AsanasFragmentDirections.navToAsanaFragment(
                selectedAsana.url,
                selectedAsana.title
            )
        )
    }
}