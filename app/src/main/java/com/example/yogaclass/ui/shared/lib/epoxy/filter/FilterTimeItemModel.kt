package com.example.yogaclass.ui.shared.lib.epoxy.filter

import android.widget.NumberPicker
import android.widget.RadioButton
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.yogaclass.R
import com.example.yogaclass.ui.shared.lib.epoxy.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.list_item_backdrop_lesson)
abstract class FilterTimeItemModel: EpoxyModelWithHolder<FilterTimeItemHolder>() {
    @EpoxyAttribute
    lateinit var title: String

    @EpoxyAttribute
    var selected: Boolean = false

    @EpoxyAttribute
    lateinit var filterSelectedAction: () -> Unit

    @EpoxyAttribute
    lateinit var minSelectedAction: (newVal: Int) -> Unit

    @EpoxyAttribute
    lateinit var secSelectedAction: (newVal: Int) -> Unit

    @EpoxyAttribute
    var minutes: Int = 0

    @EpoxyAttribute
    var seconds: Int = 0

    override fun bind(holder: FilterTimeItemHolder) {
        holder.radio.text = title
        holder.radio.setOnClickListener {
            selected = !selected
            filterSelectedAction()
        }
        holder.radio.isChecked = selected

        holder.numpicMinutes.isEnabled = selected
        holder.numpicMinutes.minValue = 0
        holder.numpicMinutes.maxValue = 59
        holder.numpicMinutes.value = minutes
        holder.numpicMinutes.setOnValueChangedListener { _, _, newVal ->
            minSelectedAction(newVal) }

        holder.numpicSeconds.isEnabled = selected
        holder.numpicSeconds.minValue = 1
        holder.numpicSeconds.maxValue = 59
        holder.numpicSeconds.value = seconds
        holder.numpicSeconds.setOnValueChangedListener { _, _, newVal ->
            secSelectedAction(newVal) }
    }
}

class FilterTimeItemHolder : KotlinEpoxyHolder() {
    val radio by bind<RadioButton>(R.id.checkbox)
    val numpicMinutes by bind<NumberPicker>(R.id.numpic_minutes)
    val numpicSeconds by bind<NumberPicker>(R.id.numpic_seconds)
}