package com.example.yogaclass.ui.lessons

data class ClockViewModel(
    var secondsPassed: Long,
    var secondsLeft: Long,
    var secondsFullTime: Long
)