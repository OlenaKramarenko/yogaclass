package com.example.yogaclass.ui.shared.lib.epoxy.filter

import android.widget.RadioButton
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.example.yogaclass.R
import com.example.yogaclass.ui.shared.lib.epoxy.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.list_item_backdrop_asanas)
abstract class FilterItemModel : EpoxyModelWithHolder<FilterItemHolder>() {
    @EpoxyAttribute
    lateinit var title: String

    @EpoxyAttribute
    var selected: Boolean = false

    @EpoxyAttribute
    lateinit var action: () -> Unit

    override fun bind(holder: FilterItemHolder) {
        holder.radio.text = title
        holder.radio.setOnClickListener {
            selected = !selected
            action()
        }
        holder.radio.isChecked = selected
    }

}

class FilterItemHolder : KotlinEpoxyHolder() {
    val radio by bind<RadioButton>(R.id.checkbox)
}

