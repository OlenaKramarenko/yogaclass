package com.example.yogaclass.ui.lessons

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import com.example.yogaclass.AppConstants.Lesson.LABEL_FILTER_SWITCH
import com.example.yogaclass.R
import com.example.yogaclass.enums.ClockState
import com.example.yogaclass.enums.ClockType
import com.example.yogaclass.model.Asana
import com.example.yogaclass.ui.base.BaseFilterFragment
import com.example.yogaclass.ui.lessons.adapter.LessonWorkOutAdapter
import com.example.yogaclass.ui.shared.ext.gone
import com.example.yogaclass.ui.shared.ext.visible
import com.example.yogaclass.ui.shared.listener.ViewPager2Attacher
import com.example.yogaclass.ui.shared.model.FilterItem
import kotlinx.android.synthetic.main.fragment_lesson.*
import kotlinx.android.synthetic.main.fragment_lesson.view.*
import kotlinx.android.synthetic.main.list_item_backdrop_lesson.*
import kotlinx.android.synthetic.main.view_backdrop_asanas.*
import kotlinx.android.synthetic.main.view_counter_pager.*
import kotlinx.android.synthetic.main.view_progressbar.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.viewModel
import timber.log.Timber


class LessonFragment: BaseFilterFragment() {

    private val _lessonViewModel: LessonViewModel by viewModel()

    val args: LessonFragmentArgs by navArgs()

    override val fragmentLayoutId: Int = R.layout.fragment_lesson

    private var _viewPagerAdapter: LessonWorkOutAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView(view)

        subscribeViewModel()

        _lessonViewModel.getLesson(args.lessonId)

    }

    private fun initView(view: View) {

        mainActivity!!.setSupportActionBar(view.tlb)
        mainActivity!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mainActivity!!.supportActionBar!!.title = args.lessonTitle

    }

    private fun subscribeViewModel() {
        subscribeLoading()
        subscribeLesson()
    }

    private fun subscribeLoading() {
        _lessonViewModel.loadingState.observe(this, Observer { isLoading ->
            if (isLoading){
                v_progress.visible()
            } else {
                v_progress.gone()
            }
        })
    }

    private fun subscribeLesson() {
        _lessonViewModel.asanaListState.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                initDataList(it)
            } else {
                Toast.makeText(this.context, getString(R.string.message_no_data),
                    Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun initDataList(asanas: List<Asana>){
        if (_viewPagerAdapter == null) {
            val asanaWorkOuts = _lessonViewModel.getAsanaWorkOutList(asanas)

            txt_amount_view.text = asanas.count().toString()
            _viewPagerAdapter = LessonWorkOutAdapter(mainActivity!!.applicationContext,
                asanaWorkOuts, _lessonViewModel.getSelectedClockType(),
                ClockState.Stop
            ) { startPauseTimer()}

            view_pager.adapter = _viewPagerAdapter
            view_pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

                override fun onPageSelected(position: Int) {
                    this@LessonFragment.onPageSelected(position)
                }
            })

            indicator.attachToPager(view_pager, ViewPager2Attacher())

            checkIfFullImagesExist(asanaWorkOuts)

            subscribeClockLiveData()
            subscribeClockState()
            subscribeClockForRestLiveData()
            subscribeClockForRestState()
        } else{
            _viewPagerAdapter?.setData(_lessonViewModel.getAsanaWorkOutList(asanas))
        }
    }

    private fun subscribeClockLiveData() {
        _lessonViewModel.clockViewModel.observe(this, Observer {
            updateTimerView(it)
        })
    }

    private fun updateTimerView(clockViewModel: ClockViewModel){
        _viewPagerAdapter?.updateClockForCurrentItem(clockViewModel, view_pager.currentItem)

        if(_lessonViewModel.getSelectedClockType() == ClockType.TIMER &&
            clockViewModel.secondsLeft == 0.toLong()) {
            showNextAsana()
        }
    }

    private var _isForcedPageChanging: Boolean = false
    private fun showNextAsana(){
        val page = view_pager.currentItem
        val isLastPage = (view_pager.adapter!!.itemCount - 1) == page

        if(!isLastPage) {
            _isForcedPageChanging = true
            view_pager.currentItem = page + 1

            _lessonViewModel.showNextAsana()
            Timber.d("--------------------- LessonFragment.showNextAsana ${page + 1} ---------------------------------------")
        }
    }

    private fun subscribeClockState() {
        _lessonViewModel.clockState.observe(this, Observer {
            updateTimerState(it)
        })
    }

    private fun updateTimerState(state: ClockState){
        _viewPagerAdapter?.changeClockState(state)
    }

    private fun subscribeClockForRestLiveData() {
        _lessonViewModel.clockForRestViewModel.observe(this, Observer {
            updateTimerForRestView(it)
        })
    }

    private fun updateTimerForRestView(clockViewModel: ClockViewModel) {
        _viewPagerAdapter?.updateRestClockForCurrentItem(clockViewModel, view_pager.currentItem)

        if(clockViewModel.secondsLeft == 0.toLong()) {
            Timber.d("--------------------- LessonFragment.updateTimerForRestView secondsLeft ${clockViewModel.secondsLeft} ---------------------------------------")
            _lessonViewModel.resetTimerForRestToInitial()
            _lessonViewModel.startClockService()
        }
    }

    private fun subscribeClockForRestState() {
        _lessonViewModel.clockForRestState.observe(this, Observer {
            updateTimerForRestState(it)
        })
    }

    private fun updateTimerForRestState(state: ClockState){
        _viewPagerAdapter?.changeClockForRestState(state)
    }

    private fun startPauseTimer(){
        _lessonViewModel.startPauseClock()
    }

    private fun onPageSelected(position: Int){
        txt_number_visible_view.text = (position + 1).toString()

        if(!_isForcedPageChanging) {
            _lessonViewModel.stopClock()
        }

        _isForcedPageChanging = false
    }

    private val jobLesson = SupervisorJob()
    private val _bgScope = CoroutineScope(Dispatchers.IO + jobLesson)
    private fun checkIfFullImagesExist(asanaWorkOuts: MutableList<AsanaWorkOutViewModel>){
        asanaWorkOuts.forEach { asanaWorkOut ->
             if (asanaWorkOut.imageUrl.isBlank()) {
                  _bgScope.launch {
                      _lessonViewModel.getFullAsanaWorkOut(asanaWorkOut).imageUrl
                  }
             }
        }
    }

    override fun initFilters(){
        super.initFilters()

        _lessonViewModel.createClockService()
        _lessonViewModel.createClockServiceForRest()

        _lessonViewModel.initFilters()

        subscribeFilters()
    }

    private fun subscribeFilters() {
        _lessonViewModel.listFilterItems.observe(this, Observer {
            setFilterData(it)
        })
    }

    private fun setFilterData(filters: List<FilterItem>){
        txt_caption.visible()
        txt_caption.text = LABEL_FILTER_SWITCH

        filterController?.funcMinutes = ::onMinutesUpdated
        filterController?.funcSeconds = ::onSecondsUpdated
        filterController?.setData(filters)
    }

    override fun updateContentBy(selectedFilterItem: Int) {
        _lessonViewModel.changeSelectedClockType(ClockType.getFilterBy(selectedFilterItem))
        _lessonViewModel.updateFilters(selectedFilterItem)

        _viewPagerAdapter?.changeClockType(_lessonViewModel.getSelectedClockType())

        if(_lessonViewModel.getSelectedClockType() == ClockType.TIMER)
            setTimerDuration(numpic_minutes.value.toLong(), numpic_seconds.value.toLong())
        else if(_lessonViewModel.getSelectedClockType() == ClockType.STOPWATCH)
            setTimerDuration(0, 0)
    }

    private fun setTimerDuration(minNewValue: Long? = null, secNewValue: Long? = null){
        _lessonViewModel.changeClockTime(minNewValue, secNewValue)
    }

    private fun onMinutesUpdated(newVal: Int){
        setTimerDuration(minNewValue = newVal.toLong())
    }

    private fun onSecondsUpdated(newVal: Int){
        setTimerDuration(secNewValue = newVal.toLong())
    }
}