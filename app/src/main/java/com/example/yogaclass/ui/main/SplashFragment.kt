package com.example.yogaclass.ui.main

import com.example.yogaclass.R
import com.example.yogaclass.ui.base.BaseFragment

class SplashFragment: BaseFragment() {
    override val fragmentLayoutId = R.layout.splash_screen
}