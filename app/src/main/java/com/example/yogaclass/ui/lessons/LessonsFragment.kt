package com.example.yogaclass.ui.lessons

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyItemSpacingDecorator
import com.example.yogaclass.AppConstants.LABEL_SORT
import com.example.yogaclass.AppConstants.TAB_LESSONS
import com.example.yogaclass.R
import com.example.yogaclass.enums.LessonsFilterType
import com.example.yogaclass.model.Lesson
import com.example.yogaclass.ui.base.BaseFilterFragment
import com.example.yogaclass.ui.lessons.epoxy.lessonList
import com.example.yogaclass.ui.shared.ext.gone
import com.example.yogaclass.ui.shared.ext.visible
import com.example.yogaclass.ui.shared.lib.epoxy.withModels
import com.example.yogaclass.ui.shared.manager.FABActionManager
import com.example.yogaclass.ui.shared.model.FilterItem
import kotlinx.android.synthetic.main.fragment_asanas.*
import kotlinx.android.synthetic.main.view_backdrop_asanas.*
import kotlinx.android.synthetic.main.view_progressbar.*
import org.koin.androidx.viewmodel.ext.viewModel


class LessonsFragment : BaseFilterFragment() {

    private val _lessonsViewModel: LessonsViewModel by viewModel()

    override val fragmentLayoutId: Int = R.layout.fragment_asanas

    private lateinit var _fabManager: FABActionManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

        subscribeViewModel()

        _lessonsViewModel.getLessons(_lessonsViewModel.selectedFilterType)

    }

    private fun initView(){
        tlb.title = TAB_LESSONS
    }

    private fun subscribeViewModel() {
        subscribeLoading()
        subscribeLessons()
    }

    private fun subscribeLoading() {
        _lessonsViewModel.loadingState.observe(this, Observer { isLoading ->
            if (isLoading){
                v_progress.visible()
            } else {
                v_progress.gone()
            }
        })
    }

    var isCustomLessonExist: Boolean = true
    private fun checkFABActionable() : Boolean {
        return isCustomLessonExist
    }

    private fun createLesson(){

    }

    private fun updateLesson(){

    }

    private fun subscribeLessons() {
        _lessonsViewModel.lessonsState.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                initDataList(it)
                //isCustomLessonExist = it.any { lesson -> lesson.lessonType == LessonType.CUSTOM }
            } else {
                Toast.makeText(this.context, getString(R.string.message_no_data),
                    Toast.LENGTH_SHORT).show()
            }
        })
    }

    private var data: MutableList<Lesson> = mutableListOf()
    private fun initDataList(lessons: List<Lesson>){

        this.data.clear()
        this.data.addAll(lessons)

        if (rv_assans.adapter == null) {
            rv_assans?.apply {
                addItemDecoration(EpoxyItemSpacingDecorator(resources.getDimension(R.dimen.app_space_md).toInt()))
                layoutManager = LinearLayoutManager(mainActivity)
                withModels {
                        data.forEach {
                            lessonList {
                                id(it.id)
                                title(it.title)
                                asanas(it.asanas ?: mutableListOf())
                                action { navigateToLesson(it) }
                            }
                        }
                }
            }
        } else{
            rv_assans.requestModelBuild()
        }
    }

    private fun navigateToLesson(selectedLesson: Lesson){
        val action = LessonsFragmentDirections.navToLessonFragment(selectedLesson.id, selectedLesson.title)

        this.findNavController().navigate(action)
    }

    override fun initFilters(){
        super.initFilters()

        _lessonsViewModel.initFilters()

        subscribeFilters()
    }

    private fun subscribeFilters() {
        _lessonsViewModel.listFilterItems.observe(this, Observer {
            setFilterData(it)
        })
    }

    private fun setFilterData(filters: List<FilterItem>){
        txt_caption.visible()
        txt_caption.text = LABEL_SORT

        filterController?.setData(filters)
    }

    override fun updateContentBy(selectedFilterItem: Int) {
        _lessonsViewModel.selectedFilterType = LessonsFilterType.getFilterBy(selectedFilterItem)

        _lessonsViewModel.getLessons(_lessonsViewModel.selectedFilterType)
        _lessonsViewModel.updateFilters(selectedFilterItem)
    }
}