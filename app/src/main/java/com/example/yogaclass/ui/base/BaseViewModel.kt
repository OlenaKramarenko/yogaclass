package com.example.yogaclass.ui.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()