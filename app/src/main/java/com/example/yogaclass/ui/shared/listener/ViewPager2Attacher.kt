package com.example.yogaclass.ui.shared.listener

import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import ru.tinkoff.scrollingpagerindicator.ScrollingPagerIndicator

class ViewPager2Attacher: ScrollingPagerIndicator.PagerAttacher<ViewPager2> {
    private lateinit var dataObserver: RecyclerView.AdapterDataObserver
    private lateinit var onPageChangeListener: ViewPager2.OnPageChangeCallback
    private lateinit var pager: ViewPager2
    private lateinit var attachedAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>

    override fun detachFromPager() {
        attachedAdapter.unregisterAdapterDataObserver(dataObserver)
        pager.unregisterOnPageChangeCallback(onPageChangeListener)
    }

    override fun attachToPager(indicator: ScrollingPagerIndicator, pager: ViewPager2) {
        attachedAdapter = pager.adapter as RecyclerView.Adapter<RecyclerView.ViewHolder>
        /*if (attachedAdapter == null) {
            throw IllegalStateException("Set adapter before call attachToPager() method");
        }*/

        this.pager = pager;

        indicator.setDotCount(attachedAdapter.itemCount);
        indicator.setCurrentPosition(pager.currentItem);

        dataObserver = object: RecyclerView.AdapterDataObserver() {

            override fun onChanged() {
                indicator.reattach()
            }

            /*
            override fun onInvalidated() {
                onChanged()
            }*/
        }

        attachedAdapter.registerAdapterDataObserver(dataObserver);

        onPageChangeListener = object: ViewPager2.OnPageChangeCallback() {
            var idleState: Boolean = true;

            override fun onPageSelected(position: Int) {
                if (idleState) {
                    indicator.setDotCount(attachedAdapter.itemCount)
                    indicator.setCurrentPosition(pager.currentItem)
                }
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                val offset: Float
                // ViewPager may emit negative positionOffset for very fast scrolling
                if (positionOffset < 0) {
                    offset = 0f;
                } else if (positionOffset > 1) {
                    offset = 1f;
                } else {
                    offset = positionOffset;
                }
                indicator.onPageScrolled(position, offset)
            }

            override fun onPageScrollStateChanged(state: Int) {
                idleState = state == ViewPager.SCROLL_STATE_IDLE
            }

        }

        pager.registerOnPageChangeCallback(onPageChangeListener)
    }

}