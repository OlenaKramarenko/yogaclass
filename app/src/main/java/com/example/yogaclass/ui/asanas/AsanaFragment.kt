package com.example.yogaclass.ui.asanas

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.yogaclass.R
import com.example.yogaclass.model.Asana
import com.example.yogaclass.ui.asanas.epoxy.asanaInstruction
import com.example.yogaclass.ui.base.BaseFragment
import com.example.yogaclass.ui.shared.ext.gone
import com.example.yogaclass.ui.shared.ext.loadWithProgress
import com.example.yogaclass.ui.shared.lib.epoxy.withModels
import kotlinx.android.synthetic.main.fragment_asana.*
import kotlinx.android.synthetic.main.fragment_asana.view.*
import org.koin.androidx.viewmodel.ext.viewModel

class AsanaFragment : BaseFragment() {
    val args: AsanaFragmentArgs by navArgs()

    private val _asanaViewModel: AsanaViewModel by viewModel()

    override val fragmentLayoutId: Int = R.layout.fragment_asana

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView(view)

        _asanaViewModel.getAsana(args.asanaUrl)

        subscribeAsanas()
    }

    private fun initView(view: View) {
        mainActivity?.setSupportActionBar(view.tlb)
        mainActivity?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mainActivity?.supportActionBar?.title = ""
        view.txt_title.text = args.asanaTitle
    }

    private fun subscribeAsanas() {
        _asanaViewModel.asanaState.observe(viewLifecycleOwner, Observer {
            if (it.completedSuccessfully()) {
                initContent(it.data)
            }
        })
    }

    private fun initContent(asana: Asana?) {
        iv_asana.loadWithProgress(
            asana?.imageUrl ?: "",
            v_progress,
            ::startPostponedEnterTransition
        )

        if (asana?.sanskritName == "") {
            con_sanskrit.gone()
        } else
            txt_sankrit_name.text = asana?.sanskritName

        if (asana?.extraName == "") {
            con_extra_name.gone()
        } else
            txt_second_name.text = asana?.extraName

        rv_instruction.layoutManager = LinearLayoutManager(activity)
        rv_instruction.withModels {
            asana?.instruction?.forEach {
                asanaInstruction {
                    id(it)
                    text(it)
                }
            }
        }
    }
}