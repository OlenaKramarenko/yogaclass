package com.example.yogaclass.ui.lessons

class AsanaWorkOutViewModel(
    var asanaId: String,
    val title: String,
    val imageUrl: String,
    var secLeft: Long,
    var url: String,
    var secPassed: Long = 0,
    var secFullTime: Long = 0,
    var secRestFullTime: Int = 0,
    var secLeftForRest: Int = 0
    )

