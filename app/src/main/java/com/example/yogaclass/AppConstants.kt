package com.example.yogaclass

object AppConstants {

    const val TAB_ASANAS = "Asanas"

    const val TAB_LESSONS = "Lessons"

    const val TITLE_SETTINGS = "Settings"

    const val LABEL_SORT = "Sort"

    const val APP_DATABASE_NAME = "YogaClass.db"

    const val KEY_SAVED_CATEGORIES = "SavedCategories"

    const val PERCENTAGE_TO_SHOW_CONTENT_IN_TOOLBAR = 20

    object Settings
    {
        const val DEFAULT_APP_THEME_ID = "0"
        const val THEME_ID_LAVENDER = "1"
        const val THEME_ID_PURPLE = "2"
        const val THEME_ID_BASIL = "3"
        const val THEME_ID_RED = "4"

        const val SETTINGS_TIME_FOR_REST_0_ID = "0"
        const val SETTINGS_TIME_FOR_REST_1_ID = "1"
        const val SETTINGS_TIME_FOR_REST_2_ID = "2"
        const val SETTINGS_TIME_FOR_REST_3_ID = "3"
        const val SETTINGS_TIME_FOR_REST_4_ID = "4"

        const val SETTINGS_SECONDS_FOR_REST_0 = 10
        const val SETTINGS_SECONDS_FOR_REST_1 = 15
        const val SETTINGS_SECONDS_FOR_REST_2 = 20
        const val SETTINGS_SECONDS_FOR_REST_3 = 25
        const val SETTINGS_SECONDS_FOR_REST_4 = 30
    }

    object Asanas{
        const val TITLE_ESSENTIAL_YOGA_POSES = "For Newbies and Veteran Yogis Alike: 50 Essential Yoga Poses"
        const val TITLE_BEGINNER_YOGA_SEQUENCE = "A Truly Beginner Yoga Sequence to Stretch You Out"
        const val TITLE_LONG_AND_LEAN_FULL_BODY = "Long and Lean Full-Body Yoga Flow"
        const val TITLE_ALL = "All"
    }

    object Asana{
        const val STR_SANSKRIT_NAME = "Sanskrit Name:"
        const val STR_ALSO_CALLED = "Also Called:"
    }

    object Lessons {
        const val TITLE_FILTER_BY_NAME = "by name"
        const val TITLE_FILTER_BY_CREATION_DATE = "by creation date"
        const val TITLE_FILTER_BY_LAST_USAGE_DATE = "by last usage date"
    }

    object Lesson {
        const val LABEL_FILTER_SWITCH = "Would you like to switch asanas"
        const val TITLE_FILTER_MANUALLY = "MANUALLY"
        const val TITLE_FILTER_TIMER = "USING TIMER"
    }

    object Network{

        object AsanaList {
            // Asanas list urls
            const val LINK_FITNESS = "https://www.popsugar.com/fitness/"
            const val LINK_ESSENTIAL_YOGA_POSES = "Most-Common-Yoga-Poses-Pictures-23102825"
            const val LINK_BEGINNER_YOGA_SEQUENCE = "Gentle-Yoga-Sequence-42422534"
            const val LINK_LONG_AND_LEAN_FULL_BODY = "Basic-Yoga-Sequence-33065422"

            const val SELECTOR_LIST = ".image-thumb"
            const val SELECTOR_URL_VALUE = "a"
            const val SELECTOR_URL_ATTR = "href"
            const val SELECTOR_VALUE = "a > img"
            const val SELECTOR_TITLE_ATTR = "title"
            const val SELECTOR_IMAGE_URL_ATTR = "src"
        }

        object Asana {
            const val SELECTOR_IMAGE_URL_VALUE = ".contents > img"
            const val SELECTOR_IMAGE_URL_ATTR = "src"
            const val SELECTOR_TITLE_VALUE = ".title-wrap > h2"
            const val SELECTOR_LIST_NAMES = ".slide-body > p"
            const val SELECTOR_LIST_INSTRUCTIONS = ".slide-body > ul > li"
        }
    }

    object Error{
        const val CODE_SERVER_ERROR = 100
        const val STR_SERVER_ERROR = "Server error"
        const val CODE_CONNECTION_ERROR = 200
        const val STR_CONNECTION_ERROR = "Connection error"
        const val CODE_UNKNOWN_ERROR = 300
        const val STR_UNKNOWN_ERROR = "Unknown error"
        const val CODE_HTTP_ERROR = 400
        const val STR_HTTP_ERROR = "HTTP error"
    }
}