package com.example.yogaclass.util

import android.os.SystemClock
import com.example.yogaclass.util.contract.Timer


abstract class Stopwatch(
    private val _interval: Long
): Timer() {

    @Synchronized override fun start() {
        cancelled = false
        startTime = SystemClock.elapsedRealtime()

        handler.sendMessage(handler.obtainMessage(msg))
    }

    override fun handleTimeCounting(){
        synchronized(this@Stopwatch) {
            if (cancelled) {
                return
            }

            val lastTickStart = SystemClock.elapsedRealtime()
            onTick(lastTickStart)

            // take into account user's onTick taking time to execute
            val lastTickDuration = SystemClock.elapsedRealtime() - lastTickStart
            var delay: Long

            delay = _interval - lastTickDuration

            // special case: user's onTick took more than interval to
            // complete, skip to next interval
            while (delay < 0) delay += _interval

            handler.sendMessageDelayed(handler.obtainMessage(msg), delay)
        }
    }
}