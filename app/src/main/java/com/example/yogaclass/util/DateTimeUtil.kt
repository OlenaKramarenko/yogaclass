package com.example.yogaclass.util

import java.util.concurrent.TimeUnit

object DateTimeUtil {
    fun getSecondsFromMinutes(minutes: Long): Long =  minutes * 60

    fun getSecondsFromMillis(millis: Long): Long = TimeUnit.MILLISECONDS.toSeconds(millis)

    fun getMillisFromSeconds(seconds: Long): Long = TimeUnit.SECONDS.toMillis(seconds)
}