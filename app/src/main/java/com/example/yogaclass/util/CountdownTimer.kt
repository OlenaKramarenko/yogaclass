package com.example.yogaclass.util

import android.os.SystemClock
import com.example.yogaclass.util.contract.Timer
import timber.log.Timber

abstract class CountdownTimer(
    var millisInFuture: Long,
    var countdownInterval: Long
): Timer() {

    private var stopTimeInFuture: Long = 0

    @Synchronized override fun start() {
        cancelled = false
        //Timber.e("---------------------  CountdownTimer.start $millisInFuture --------------------------------")
        if (millisInFuture <= 0) {
            onFinish()
            return
        }

        startTime = SystemClock.elapsedRealtime()
        stopTimeInFuture = SystemClock.elapsedRealtime() + millisInFuture
        handler.sendMessage(handler.obtainMessage(msg))
        //Timber.e("---------------------  CountdownTimer.start $handler ---------------------------------------")
    }

    override fun handleTimeCounting() {
        synchronized(this@CountdownTimer) {
            //Timber.e("---------------------  CountdownTimer.handleTimeCounting ---------------------------------------")
            if (cancelled) {
                return
            }

            val millisLeft = stopTimeInFuture - SystemClock.elapsedRealtime()

            if (millisLeft <= 0) {
                onFinish()
            } else {
                val lastTickStart = SystemClock.elapsedRealtime()
                onTick(lastTickStart, millisLeft)

                // take into account user's onTick taking time to execute
                val lastTickDuration = SystemClock.elapsedRealtime() - lastTickStart
                var delay: Long

                if (millisLeft < countdownInterval) {
                    // just delay until done
                    delay = millisLeft - lastTickDuration

                    // special case: user's onTick took more than interval to
                    // complete, trigger onFinish without delay
                    if (delay < 0) delay = 0
                } else {
                    delay = countdownInterval - lastTickDuration

                    // special case: user's onTick took more than interval to
                    // complete, skip to next interval
                    while (delay < 0) delay += countdownInterval
                }

                handler.sendMessageDelayed(handler.obtainMessage(msg), delay)
            }
        }
    }
}