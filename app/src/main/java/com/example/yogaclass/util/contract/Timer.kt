package com.example.yogaclass.util.contract

import android.os.Handler
import android.os.Message
import timber.log.Timber
import java.lang.ref.WeakReference

abstract class Timer {
    protected var cancelled: Boolean = false

    protected var startTime: Long = 0

    @Synchronized fun cancel() {
        cancelled = true
        handler.removeMessages(msg)
    }

    abstract fun start()

    abstract fun onTick(signalTime: Long, millisLeft: Long = 0)

    abstract fun onFinish()

    protected var msg: Int = 1

    protected abstract val timerRef: WeakReference<Timer>

    protected abstract var handler: TimerHandler

    abstract fun handleTimeCounting()

    class TimerHandler(private val stopwatchRef: WeakReference<Timer>) : Handler() {

        override fun handleMessage(msg: Message?) {
            //Timber.e("---------------------  Timer handleMessage ---------------------------------------")
            stopwatchRef.get()?.handleTimeCounting()
        }
    }
}