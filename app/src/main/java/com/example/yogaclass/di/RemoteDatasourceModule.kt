package com.example.yogaclass.di

import com.example.yogaclass.AppConstants.Network.AsanaList.LINK_FITNESS
import com.example.yogaclass.network.services.DataService
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import org.koin.dsl.module
import pl.droidsonroids.retrofit2.JspoonConverterFactory
import retrofit2.Retrofit

val remoteDataSourceModule = module {
    single { createWebService<DataService>(LINK_FITNESS) }
}

inline fun <reified T> createWebService(baseUrl: String): T {
    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(JspoonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build().create(T::class.java)
}

