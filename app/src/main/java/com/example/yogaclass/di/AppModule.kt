package com.example.yogaclass.di

import androidx.room.Room
import com.example.yogaclass.AppConstants.APP_DATABASE_NAME
import com.example.yogaclass.dal.db.AppDatabase
import com.example.yogaclass.dal.db.entity.AsanaEntity
import com.example.yogaclass.dal.db.mapper.AsanaEntityMapperImpl
import com.example.yogaclass.dal.db.mapper.LessonEntityMapperImpl
import com.example.yogaclass.dal.prefs.SharedPreferenceManager
import com.example.yogaclass.dal.repository.AsanaRepositoryImpl
import com.example.yogaclass.dal.repository.LessonRepositoryImpl
import com.example.yogaclass.domain.manager.settings.SettingsManager
import com.example.yogaclass.domain.manager.settings.SettingsManagerImpl
import com.example.yogaclass.domain.mapper.BaseMapper
import com.example.yogaclass.domain.mapper.BaseMapper2
import com.example.yogaclass.domain.repository.AsanaRepository
import com.example.yogaclass.domain.repository.LessonRepository
import com.example.yogaclass.model.Asana
import com.example.yogaclass.network.converter.AsanaDtoMapperImpl
import com.example.yogaclass.network.converter.AsanaListMapperImpl
import com.example.yogaclass.network.converter.AsanasDtoMapperImpl
import com.example.yogaclass.network.dto.AsanaListDto
import com.example.yogaclass.ui.asanas.AsanaViewModel
import com.example.yogaclass.ui.asanas.AsanasViewModel
import com.example.yogaclass.ui.lessons.LessonViewModel
import com.example.yogaclass.ui.lessons.LessonsViewModel
import com.example.yogaclass.ui.main.MainViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single {
        Room.databaseBuilder(
            androidApplication(),
            AppDatabase::class.java,
            APP_DATABASE_NAME
        )
            .build()
    }

    factory { SharedPreferenceManager(androidContext()) }

    single { get<AppDatabase>().asanaDao() }
    single { get<AppDatabase>().lessonDao() }
    single { get<AppDatabase>().lessonAsanaDao() }

    single<AsanaRepository> { AsanaRepositoryImpl(get(), get(), get(), get(), get(), get(), get()) }
    single<LessonRepository> { LessonRepositoryImpl(get(), get(), get(), get()) }

    single<BaseMapper2<AsanaEntity, Asana>>{ AsanaEntityMapperImpl() }
    single{ LessonEntityMapperImpl() }

    single<BaseMapper<AsanaListDto, Asana>>{ AsanaListMapperImpl() }

    single{ AsanasDtoMapperImpl(get()) }
    single{ AsanaDtoMapperImpl() }

    viewModel { MainViewModel(get(), get()) }
    viewModel { AsanasViewModel(get()) }
    viewModel { LessonsViewModel(get()) }
    viewModel { AsanaViewModel(get()) }
    viewModel { LessonViewModel(get(), get(), get()) }

    single<SettingsManager> { SettingsManagerImpl(get()) }
}