package com.example.yogaclass

import android.app.Application
import com.example.yogaclass.di.appModule
import com.example.yogaclass.di.remoteDataSourceModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        if(BuildConfig.DEBUG){
            Timber.plant(Timber.DebugTree());
        }

        instance = this

        startKoin {

            androidContext(this@App)
            modules(appModule, remoteDataSourceModule)
        }
    }

    companion object {
        lateinit var instance: App
            private set
    }
}