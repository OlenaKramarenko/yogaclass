package com.example.yogaclass.enums

import com.example.yogaclass.AppConstants.Asanas.TITLE_ALL
import com.example.yogaclass.AppConstants.Asanas.TITLE_BEGINNER_YOGA_SEQUENCE
import com.example.yogaclass.AppConstants.Asanas.TITLE_ESSENTIAL_YOGA_POSES
import com.example.yogaclass.AppConstants.Asanas.TITLE_LONG_AND_LEAN_FULL_BODY

enum class AsanaCategory(var filterKey: Int) {
    ESSENTIAL_YOGA_POSES(0),
    BEGINNER_YOGA_SEQUENCE(1),
    LONG_AND_LEAN_FULL_BODY(2),
    ALL(3);

    companion object {

        fun getCategoryBy(categoryInt: Int): AsanaCategory {

            return when (categoryInt) {
                0 -> ESSENTIAL_YOGA_POSES
                1 -> BEGINNER_YOGA_SEQUENCE
                2 -> LONG_AND_LEAN_FULL_BODY
                else -> ALL
            }
        }

        fun getCategoryNameBy(categoryInt: Int): String {

            return when (categoryInt) {
                0 -> TITLE_ESSENTIAL_YOGA_POSES
                1 -> TITLE_BEGINNER_YOGA_SEQUENCE
                2 -> TITLE_LONG_AND_LEAN_FULL_BODY
                else -> TITLE_ALL
            }
        }
    }
}