package com.example.yogaclass.enums

enum class LessonsFilterType (var filterKey: Int) {
    CREATION_DATE(0),
    LAST_USAGE_DATE(1),
    NAME(2);

    companion object {

        fun getFilterBy(filterInt: Int): LessonsFilterType {

            return when (filterInt) {
                0 -> CREATION_DATE
                1 -> LAST_USAGE_DATE
                else -> NAME
            }
        }
    }
}