package com.example.yogaclass.enums

enum class ClockState(var clockStateKey: Int) {
    Stop (10),
    Start ( 20),
    Running ( 30),
    OnPause ( 40),
    Resuming ( 50)
}