package com.example.yogaclass.enums

enum class ClockType(var filterKey: Int) {
    STOPWATCH(0),
    TIMER(1);

    companion object {

        fun getFilterBy(filterInt: Int): ClockType {

            return when (filterInt) {
                1 -> TIMER
                else -> STOPWATCH
            }
        }
    }
}