package com.example.yogaclass.ext

import java.util.*

fun Date.toTimeStamp() : Long = this.time

fun Long.toDate() : Date = Date(this)
