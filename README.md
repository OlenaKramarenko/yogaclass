# YogaClass


The app which helps do yoga. It contains two main screens with lists of asanas and lessons.  
Content with asanas is parsed from a website and is saved to a database. Also, filter for asanas and lesson is included  
(for asanas - by default lessons, for lessons - by name, by creation date, by last usage date).  
A click on an asana (the screen with a list of asanas) opens a screen with description for this asana.  
A click on a lesson opens the workout screen for this lesson.  
Workout screen includes an option for switching asanas manually or using a timer.  
  
For manual option:  
a touch on a line "Press to start" starts chronometer for control the time of standing in asana.  

For timer option:  
a first touch on a line "Press to start" starts countdown-timer (default time - 20 seconds).  
When the time for this asana is up, loader shows time for rest between asanas (default 10 sec).  
After rest time is up, a new timer for next asana is started.  
  
Also it is possible to choose a theme, change default time for rest in settings.  
  
  
  
Used default components, 3rd party libs:  
-- Single Activity, Navigation Architecture Component  
-- ViewModel, LiveData  
-- Higher-Order functions and lambdas, Extensions, Coroutines  
-- Styles and themes  
-- Data storage, Room  
-- Koin, Retrofit, jspoon, Glide, Epoxy, Timber    
-- ViewPager2, Pager indicator   
-- Android Settings Preference  
-- Custom View Attributes


